import 'package:flutter/material.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:provider/provider.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/providers/menu_selected.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/home/screens/cars_screen.dart';
import 'package:starlinx_support/ui/home/screens/drivers_screen.dart';
import 'package:starlinx_support/ui/home/screens/fleets_screen.dart';
import 'package:starlinx_support/ui/home/screens/users_screen.dart';

class MenuController extends GetxController {
  RxInt _selectedIndex = 0.obs;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int get selectedIndex => _selectedIndex.value;
  List<DrawerItemData> get menuItems => _getMenuItems();
  /* [
        DrawerItemData(
            "Add Driver", Icons.add_reaction_outlined, AddDriverScreen()),
        DrawerItemData(
            "Users", Icons.supervisor_account_outlined, UsersScreen()),
        DrawerItemData("Fleets", Icons.note_alt_outlined, FleetsScreen()),
        DrawerItemData("Cars", Icons.directions_car_outlined, CarsScreen()),
        DrawerItemData(
            "Drivers", Icons.account_circle_outlined, DriversScreen()),
        //  DrawerItemData("Settings", Icons.settings_outlined, SettingsScreen())
      ];*/

  final _userService = locator<UserService>();
  List<DrawerItemData>? _menuItems;
  List<DrawerItemData> _getMenuItems() {
    if (_menuItems != null) {
      return _menuItems!;
    }

    List<DrawerItemData> res = [];
    final currentUser = _userService.currentUser;
    if (currentUser != null) {
      if (currentUser.isViewUsers()) {
        res.add(DrawerItemData(
            "Users", Icons.supervisor_account_outlined, UsersScreen()));
      }

      if (currentUser.isViewCars()) {
        res.add(DrawerItemData(
            "Vehicles", Icons.directions_car_outlined, CarsScreen()));
      }

      if (currentUser.isViewProject()) {
        res.add(
            DrawerItemData("Fleets", Icons.note_alt_outlined, FleetsScreen()));
      }

      if (currentUser.isViewDrivers()) {
        res.add(DrawerItemData(
            "Drivers", Icons.account_circle_outlined, DriversScreen()));
      }

      /*if (currentUser.isCreateDrivers()) {
        res.add(DrawerItemData(
            "Add Driver", Icons.add_reaction_outlined, AddDriverScreen()));
      }*/

    }

    _menuItems = res;
    return res;
  }

  GlobalKey<ScaffoldState> get scaffoldkey => _scaffoldKey;

  void openOrCloseDrawer() {
    if (_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.openEndDrawer();
    } else {
      _scaffoldKey.currentState!.openDrawer();
    }
  }

  bool isDrawerOpen() {
    return _scaffoldKey.currentState!.isDrawerOpen;
  }

  void setMenuIndex(BuildContext context, int index) {
    _selectedIndex.value = index;
    MenuSelectedProvider provider =
        Provider.of<MenuSelectedProvider>(context, listen: false);
    provider.changeIndex(index);

    if (_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.openEndDrawer();
    }
  }
}

class DrawerItemData {
  final String title;
  final IconData icon;
  final StatefulWidget body;
  DrawerItemData(this.title, this.icon, this.body);
}
