class DriverModel {
  int driverId;
  int projectId;
  String ident;
  String firstName;
  String lastName;
  String phoneNumber;

  DriverModel(
      {required this.driverId,
      required this.projectId,
      required this.ident,
      required this.firstName,
      required this.lastName,
      required this.phoneNumber});

  factory DriverModel.fromJson(Map<String, dynamic> responseData) {
    return DriverModel(
      driverId: responseData['driver_id'],
      projectId: (responseData['project_id'] != null
          ? responseData['project_id']
          : -1),
      ident: (responseData['ident'] != null ? responseData['ident'] : ''),
      firstName: (responseData['first_name'] != null
          ? responseData['first_name']
          : ''),
      lastName:
          (responseData['last_name'] != null ? responseData['last_name'] : ''),
      phoneNumber: (responseData['phone_number'] != null
          ? responseData['phone_number']
          : ''),
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> responseData = {
      'driverId': driverId,
      'projectId': projectId,
      'ident': ident,
      'firstName': firstName,
      'lastName': lastName,
      'phoneNumber': phoneNumber
    };

    return responseData;
  }

  String toString() {
    return '''driverId: $driverId, projectId: $projectId, ident: $ident, 
    firstName: $firstName, lastName: $lastName, 
    phoneNumber: $phoneNumber.''';
  }
}
