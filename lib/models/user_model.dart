class UserModel {
  int userId;
  String name;
  bool active;
  int roleId;
  String firstName;
  String lastName;
  int timeZone;
  String email;
  int projectId;
  List<dynamic> projectIds;

  UserModel(
      {required this.userId,
      required this.name,
      required this.active,
      required this.roleId,
      required this.firstName,
      required this.lastName,
      required this.timeZone,
      required this.email,
      required this.projectId,
      required this.projectIds});

  factory UserModel.fromJson(Map<String, dynamic> responseData) {
    return UserModel(
      userId: responseData['user_id'],
      name: responseData['user_name'],
      active: (responseData['active'] != null && responseData['active'] == 1)
          ? true
          : false,
      roleId: (responseData['role_id'] != null ? responseData['role_id'] : -1),
      firstName: (responseData['first_name'] != null
          ? responseData['first_name']
          : ''),
      lastName:
          (responseData['last_name'] != null ? responseData['last_name'] : ''),
      timeZone:
          (responseData['time_zone'] != null ? responseData['time_zone'] : -1),
      email: (responseData['email'] != null ? responseData['email'] : ''),
      projectId: (responseData['project_id'] != null
          ? responseData['project_id']
          : -1),
      projectIds: (responseData['project_ids'] != null
          ? responseData['project_ids']
          : {}),
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> responseData = {
      'userId': userId,
      'name': name,
      'active': active,
      'roleId': roleId,
      'firstName': firstName,
      'lastName': lastName,
      'timeZone': timeZone,
      'email': email,
      'projectId': projectId,
      'projectIds': projectIds
    };

    return responseData;
  }

  String toString() {
    return '''UserID: $userId, 
    Name: $name, 
    Active: $active, 
    roleId: $roleId, 
    firstName: $firstName, 
    lastName: $lastName, 
    timeZone: $timeZone, 
    email: $email, 
    projectId: $projectId''';
  }
}
