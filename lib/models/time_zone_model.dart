class TimeZoneModel {
  int zoneId;
  String zoneName;

  TimeZoneModel({required this.zoneId, required this.zoneName});

  factory TimeZoneModel.fromJson(Map<String, dynamic> responseData) {
    return TimeZoneModel(
        zoneId:
            (responseData['zone_id'] != null ? responseData['zone_id'] : -1),
        zoneName: (responseData['zone_name'] != null
            ? responseData['zone_name']
            : ''));
  }

  String toString() {
    return 'ZoneId: $zoneId, ZoneName: $zoneName';
  }
}
