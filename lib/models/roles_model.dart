class RolesModel {
  int roleId;
  String description;

  RolesModel({required this.roleId, required this.description});

  factory RolesModel.fromJson(Map<String, dynamic> responseData) {
    return RolesModel(
        roleId:
            (responseData['role_id'] != null ? responseData['role_id'] : -1),
        description: (responseData['description'] != null
            ? responseData['description']
            : ''));
  }

  String toString() {
    return 'RoleId: $roleId, ZoneName: $description';
  }
}
