class FleetModel {
  int projectId;
  String name;

  FleetModel({required this.projectId, required this.name});

  factory FleetModel.fromJson(Map<String, dynamic> responseData) {
    return FleetModel(
        projectId: (responseData['project_id'] != null
            ? responseData['project_id']
            : -1),
        name: responseData['name']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> responseData = {'projectId': projectId, 'name': name};

    return responseData;
  }

  String toString() {
    return 'projectId: $projectId, Name: $name';
  }
}
