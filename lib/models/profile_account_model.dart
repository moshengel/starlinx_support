class ProfileAccountModel {
  int userID;
  int driverID;
  String firstName;
  String lastName;
  int roleId;
  int allowCommand;
  int transmitAsStarLink;
  String country;
  Map<String, dynamic> permissions;

  ProfileAccountModel(
      {required this.userID,
      required this.driverID,
      required this.firstName,
      required this.lastName,
      required this.roleId,
      required this.allowCommand,
      required this.transmitAsStarLink,
      required this.country,
      required this.permissions});

  factory ProfileAccountModel.fromJson(Map<String, dynamic> responseData) {
    return ProfileAccountModel(
      userID: responseData['UserID'],
      driverID:
          (responseData['DriverID'] != null ? responseData['DriverID'] : -1),
      firstName: responseData['FirstName'],
      lastName: responseData['LastName'],
      roleId: responseData['RoleID'],
      allowCommand: (responseData['AllowCommand'] != null
          ? responseData['AllowCommand']
          : -1),
      transmitAsStarLink: (responseData['TransmitAsStarLink'] != null
          ? responseData['TransmitAsStarLink']
          : -1),
      country: (responseData['Country'] != null ? responseData['Country'] : ''),
      permissions: (responseData['Permissions'] != null
          ? responseData['Permissions']
          : {}),
    );
  }
  Map<String, dynamic> toJson() {
    Map<String, dynamic> responseData = {
      'UserID': userID,
      'DriverID': driverID,
      'FirstName': firstName,
      'LastName': lastName,
      'RoleID': roleId,
      'AllowCommand': allowCommand,
      'TransmitAsStarLink': transmitAsStarLink,
      'Country': country,
      'Permissions': permissions,
    };

    return responseData;
  }

  String toString() {
    var per = getPermissionsToString();
    return '''UserID: $userID, 
    driverID: $driverID, 
    firstName: $firstName, 
    lastName: $lastName, 
    roleId: $roleId, 
    allowCommand: $allowCommand, 
    TransmitAsStarLink: $transmitAsStarLink, 
    Country: $country, 
    permissions: $per''';
  }

  String getPermissionsToString() {
    String res = "";
    if (permissions.isNotEmpty) {
      for (var item in permissions.entries) {
        res += item.key + ": " + item.value.toString() + ". ";
      }
    }

    return res;
  }

  bool _isPermissions(String val) {
    return permissions.isNotEmpty &&
        permissions.containsKey(val) &&
        permissions[val] == 1;
  }

  bool isViewUsers() {
    return _isPermissions('ViewUsers');
  }

  bool isViewCars() {
    return _isPermissions('ViewCars');
  }

  bool isViewDrivers() {
    return _isPermissions('ViewDrivers');
  }

  bool isViewProject() {
    return _isPermissions('ViewProject');
  }

  bool isViewApplications() {
    return _isPermissions('ViewApplications');
  }

  bool isCreateUsers() {
    return _isPermissions('CreateUsers');
  }

  bool isCreateCars() {
    return _isPermissions('CreateCars');
  }

  bool isCreateDrivers() {
    return _isPermissions('CreateDrivers');
  }

  bool isCreateProject() {
    return _isPermissions('CreateProject');
  }

  bool isCreateApplications() {
    return _isPermissions('CreateApplications');
  }
}
