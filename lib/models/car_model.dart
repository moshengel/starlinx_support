class CarModel {
  int carId;
  String deviceId;
  int projectId;
  int timeZone;
  String carNumber;
  String carType;
  String subType;
  String carYear;
  String carColor;
  String simImsi;
  String simPhone;
  List<dynamic> driverIds;

  CarModel(
      {required this.carId,
      required this.deviceId,
      required this.projectId,
      required this.timeZone,
      required this.carNumber,
      required this.carType,
      required this.subType,
      required this.carYear,
      required this.carColor,
      required this.simImsi,
      required this.simPhone,
      required this.driverIds});

  factory CarModel.fromJson(Map<String, dynamic> responseData) {
    return CarModel(
      carId: responseData['car_id'],
      deviceId:
          (responseData['device_id'] != null ? responseData['device_id'] : ''),
      projectId: (responseData['project_id'] != null
          ? responseData['project_id']
          : -1),
      timeZone:
          (responseData['time_zone'] != null ? responseData['time_zone'] : -1),
      carNumber: (responseData['car_number'] != null
          ? responseData['car_number']
          : ''),
      carType:
          (responseData['car_type'] != null ? responseData['car_type'] : ''),
      subType:
          (responseData['sub_type'] != null ? responseData['sub_type'] : ''),
      carYear:
          (responseData['car_year'] != null ? responseData['car_year'] : ''),
      carColor:
          (responseData['car_color'] != null ? responseData['car_color'] : ''),
      simImsi:
          (responseData['sim_imsi'] != null ? responseData['sim_imsi'] : ''),
      simPhone:
          (responseData['sim_phone'] != null ? responseData['sim_phone'] : ''),
      driverIds: (responseData['driver_ids'] != null
          ? responseData['driver_ids']
          : {}),
    );
  }
  Map<String, dynamic> toJson() {
    Map<String, dynamic> responseData = {
      'carId': carId,
      'deviceId': deviceId,
      'projectId': projectId,
      'timeZone': timeZone,
      'carNumber': carNumber,
      'carType': carType,
      'subType': subType,
      'carYear': carYear,
      'carColor': carColor,
      'simImsi': simImsi,
      'simPhone': simPhone,
      'driverIds': driverIds,
    };

    return responseData;
  }

  String toString() {
    return '''carId: $carId, deviceId: $deviceId, projectId: $projectId, 
    timeZone: $timeZone, carNumber: $carNumber, carType: $carType, 
    subType: $subType, carYear: $carYear, carColor: $carColor''';
  }
}
