import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/profile_account_model.dart';

class AppPreferencesService {
  final log = getLogger('AppPreferencesService');

  Future<String?> getCurrentToken() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString("token");
    } catch (e) {
      log.e('Error:$e');
    }

    return null;
  }

  Future<void> saveToken(String token) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("token", token);
    } catch (e) {
      log.e('Error:$e');
    }

    /* if (username != null && username.isNotEmpty) {
      prefs.setString("username", username);
    }*/

    // return prefs.commit();
  }

  Future<void> removeToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      prefs.remove("token");
    } catch (e) {
      log.e('Error:$e');
    }
  }

  Future<ProfileAccountModel?> getCurrentUser() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      String? user = prefs.getString("user");
      if (user != null && user.isNotEmpty) {
        // Map<String, dynamic> s = json.decode(user);

        return ProfileAccountModel.fromJson(json.decode(user));
        // return json.decode(user);
      }
    } catch (e) {
      log.e('Error:$e');
    }

    return null;
  }

  Future<void> saveUser(ProfileAccountModel user) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString("user", json.encode(user.toJson()));
    } catch (e) {
      log.e('Error:$e');
    }

    // return prefs.commit();
  }

  Future<void> removeUser() async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.remove("user");
    } catch (e) {
      log.e('Error:$e');
    }
  }

  /* Future<String?> getUsername() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("username");
  }*/
}
