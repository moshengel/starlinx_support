import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/app/app.router.dart';
import 'package:starlinx_support/models/profile_account_model.dart';
import 'package:starlinx_support/providers/auth.dart';
import 'package:starlinx_support/services/shared_preference.dart';

class UserService {
  final log = getLogger('UserService');
  final _appPreferencesService = locator<AppPreferencesService>();
  final _navigationService = locator<NavigationService>();

  String? _currentToken;
  ProfileAccountModel? _myProfile;
  AuthProvider? _auth; // = Provider.of<AuthProvider>(context);

  String? get currentToken => _currentToken;
  ProfileAccountModel? get currentUser => _myProfile;
  bool get hasLoggedInUser =>
      _currentToken != null && _currentToken!.isNotEmpty;

  void setSessionId(String sessionId) async {
    _currentToken = sessionId;
    await _appPreferencesService.saveToken(sessionId);
  }

  void setProfile(String sessionId, ProfileAccountModel myProfile) async {
    setSessionId(sessionId);
    _myProfile = myProfile;
    await _appPreferencesService.saveUser(myProfile);
    changeStatus(Status.LoggedIn);
    _navigationService.clearStackAndShow(Routes.homeView);
  }

  void changeStatus(Status status) {
    if (_auth != null) {
      _auth!.changeStatus(status);
    }
  }

  Future<void> loadingData(BuildContext context) async {
    _auth = Provider.of<AuthProvider>(context, listen: false);
    // await _appPreferencesService.removeToken();
    _currentToken = await _appPreferencesService.getCurrentToken();
    log.i('currentToken:$_currentToken');
    if (hasLoggedInUser) {
      changeStatus(Status.LoggedIn);
    }
    _myProfile = await _appPreferencesService.getCurrentUser();
    // print(_myProfile);
    log.i('currentUser:$_myProfile');
  }

  void logout(String? msg, bool isLogoutView) async {
    await _appPreferencesService.removeToken();
    await _appPreferencesService.removeUser();

    _currentToken = null;
    _myProfile = null;

    changeStatus(Status.NotLoggedIn);

    if (isLogoutView) {
      _navigationService.clearStackAndShow(Routes.loginView);
    }
    // _navigationService.clearStackAndShow(Routes.loginView);
  }

  /*Future<String?> getUsername() async {
    final String? username = await _appPreferencesService.getUsername();
    if (!hasLoggedInUser || username == null || username.isEmpty) {
      logout(null, true);

      return null;
    }
    log.v('Sync user $username');
    return username;
    // _currentUser = await _userApi.getUserAccount(username);
  }*/

  /*Map<String, dynamic> checkIsAuth(responseData) {
    Map<String, dynamic> result = {'status': false, 'message': ''};
    if (responseData['cmd'] == 'login') {
      if (responseData['error'] != null) {
        result['message'] = responseData['error'];
      } else if (responseData['session_id'] != null &&
          responseData['session_id'].toString().length > 0) {
        // UserPreferences().setSessionId(responseData['session_id']);

        result['status'] = true;
        result['message'] = responseData['session_id'];
      }
    }

    return result;
  }

  Future<Map<String, dynamic>> login(
      String username, String password, BuildContext context) async {
    AuthProvider auth = Provider.of<AuthProvider>(context, listen: false);
    auth.changeStatus(Status.Authenticating);

    final List<Map<String, String>> formData = [
      {"cmd": "login", "user": username, "password": password}
    ];
    // String formDataString = 'qL=1&Debug=1&qT=Login&u=$username&p=$password';

    http.Response response = await http.post(
      _baceUrl,
      // body: formDataString,
      body: json.encode(formData),
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'GET, HEAD'
      },
    );

    Map<String, dynamic> result = {'status': false, 'message': ''};
    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      if (responseData.length > 0) {
        result = checkIsAuth(responseData[0]);
      }
    } else {
      result = {'status': false, 'message': 'response'};
    }

    if (result['status'] != null && result['status'] == true) {
      auth.changeStatus(Status.LoggedIn);
      String sessionId = result['message'];
      _currentToken = sessionId;
      await _appPreferencesService.saveToken(sessionId, username);
    } else {
      auth.changeStatus(Status.NotLoggedIn);
    }

    return result;
  }

*/
}
