import 'package:flutter/material.dart';

enum Status { NotLoggedIn, LoggedIn, Authenticating, LoggedOut }

class AuthProvider with ChangeNotifier {
  // Uri _baceUrl = Uri.parse(AppUrl.baseURL);
  Status _loggedInStatus = Status.NotLoggedIn;
  // Status _registeredInStatus = Status.NotRegistered;

  Status get loggedInStatus => _loggedInStatus;
  // Status get registeredInStatus => _registeredInStatus;

  changeStatus(Status status) {
    _loggedInStatus = status;
    notifyListeners();
  }

  /*Map<String, dynamic> checkIsAuth(responseData) {
    Map<String, dynamic> result = {'status': false, 'message': ''};
    if (responseData['cmd'] == 'login') {
      if (responseData['error'] != null) {
        result['message'] = responseData['error'];
      } else if (responseData['session_id'] != null &&
          responseData['session_id'].toString().length > 0) {
        // UserPreferences().setSessionId(responseData['session_id']);

        result['status'] = true;
        result['message'] = responseData['session_id'];
      }
    }

    return result;
  }
  Future<Map<String, dynamic>> login(String username, String password) async {
    changeStatus(Status.Authenticating);

    final List<Map<String, String>> formData = [
      {"cmd": "login", "user": username, "password": password}
    ];

    http.Response response = await http.post(
      _baceUrl,
      body: json.encode(formData),
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        // 'Access-Control-Allow-Origin': '*',
        // 'Access-Control-Allow-Methods': 'GET, HEAD'
      },
    );

    Map<String, dynamic> result = {'status': false, 'message': ''};
    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      if (responseData.length > 0) {
        result = checkIsAuth(responseData[0]);
      }
    } else {
      result = {'status': false, 'message': 'response'};
    }

    if (result['status'] != null && result['status'] == true) {
      changeStatus(Status.LoggedIn);
    } else {
      changeStatus(Status.NotLoggedIn);
    }

    return result;
  }
*/
  /*Future<Map<String, dynamic>> login(String email, String password) async {
    var result;

    final Map<String, dynamic> loginData = {
      'user': {
        'email': email,
        'password': password
      }
    };

    _loggedInStatus = Status.Authenticating;
    notifyListeners();

    Response response = await post(
      AppUrl.login,
      body: json.encode(loginData),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);

      var userData = responseData['data'];

      User authUser = User.fromJson(userData);

      UserPreferences().saveUser(authUser);

      _loggedInStatus = Status.LoggedIn;
      notifyListeners();

      result = {'status': true, 'message': 'Successful', 'user': authUser};
    } else {
      _loggedInStatus = Status.NotLoggedIn;
      notifyListeners();
      result = {
        'status': false,
        'message': json.decode(response.body)['error']
      };
    }
    return result;
  }*/
  /*Future<Map<String, dynamic>> register(
      String email, String password, String passwordConfirmation) async {
    final Map<String, dynamic> registrationData = {
      'user': {
        'email': email,
        'password': password,
        'password_confirmation': passwordConfirmation
      }
    };
    var url = Uri.parse(AppUrl.register);
    return await http
        .post(url,
            body: json.encode(registrationData),
            headers: {'Content-Type': 'application/json'})
        .then(onValue)
        .catchError(onError);
  }*/
  /*static Future<FutureOr> onValue(http.Response response) async {
    var result;
    final Map<String, dynamic> responseData = json.decode(response.body);

    print(response.statusCode);
    if (response.statusCode == 200) {
      var userData = responseData['data'];

      User authUser = User.fromJson(userData);

      // UserPreferences().saveUser(authUser);
      result = {
        'status': true,
        'message': 'Successfully registered',
        'data': authUser
      };
    } else {
//      if (response.statusCode == 401) Get.toNamed("/login");
      result = {
        'status': false,
        'message': 'Registration failed',
        'data': responseData
      };
    }

    return result;
  }

  static onError(error) {
    print("the error is $error.detail");
    return {'status': false, 'message': 'Unsuccessful Request', 'data': error};
  }*/
}
