import 'package:flutter/material.dart';

class MenuSelectedProvider with ChangeNotifier {
  // Uri _baceUrl = Uri.parse(AppUrl.baseURL);
  int _selectedIndex = 0;
  // Status _registeredInStatus = Status.NotRegistered;

  int get selectedIndex => _selectedIndex;
  // Status get registeredInStatus => _registeredInStatus;

  changeIndex(int index) {
    _selectedIndex = index;
    notifyListeners();
  }
}
