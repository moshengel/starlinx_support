import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:starlinx_support/providers/auth.dart';
import 'package:starlinx_support/providers/menu_selected.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';

import 'app/app.locator.dart';
import 'app/app.router.dart';

void main() {
  GestureBinding.instance?.resamplingEnabled = true;
  WidgetsFlutterBinding.ensureInitialized();

  setupLocator();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(new MyApp());
  });
  // runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final _userService = locator<UserService>();
    // _userService.loadingData(context);

    var theme = ThemeData(
      brightness: Brightness.dark,
      scaffoldBackgroundColor: mcBackgroundColor,
      primaryColor: mcPrimaryColor,
      // accentColor: mcAccentColor,
      textTheme: Theme.of(context).textTheme.apply(
            bodyColor: mcTextColor,
            displayColor: mcTextColor,
          ),
      primaryIconTheme:
          Theme.of(context).primaryIconTheme.copyWith(color: mcTextColor),
      //accentIconTheme:
      //Theme.of(context).accentIconTheme.copyWith(color: mcTextColor),
      inputDecorationTheme: InputDecorationTheme(
        // fillColor: Colors.white,
        // labelStyle: TextStyle(color: Colors.white),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(mdRadiusCircular)),
        // focusColor: primaryColor,
        //focusedBorder: OutlineInputBorder(
        // borderSide: BorderSide(color: primaryColor),
        // )
      ),
      /*inputDecorationTheme: InputDecorationTheme(
              fillColor: Colors.white,
              border: OutlineInputBorder(),
              contentPadding: EdgeInsets.symmetric(
                vertical: 22,
                horizontal: 26,
              ),
              labelStyle: TextStyle(
                color: Colors.red,
                fontSize: 35,
                decorationColor: Colors.red,
              ),
            ),*/
      primarySwatch: mcPrimarySwatch,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      /*pageTransitionsTheme: PageTransitionsTheme(
        // makes all platforms that can run Flutter apps display routes without any animation
        builders:
            Map<TargetPlatform, _InanimatePageTransitionsBuilder>.fromIterable(
                TargetPlatform.values.toList(),
                key: (dynamic k) => k,
                value: (dynamic _) => const _InanimatePageTransitionsBuilder()),
      ),*/
    );

    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => AuthProvider()),
          ChangeNotifierProvider(create: (_) => MenuSelectedProvider()),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'StarLinX Support',
          theme: theme,
          initialRoute: Routes.startUpView,

          // navigatorObservers: [AppRouteObserver()],
          /*routes: {
            Routes.startUpView: (_) => const StartUpView(),
            Routes.loginView: (_) => const Login(),
            Routes.homeView: (_) => HomeView(),
          },*/
          navigatorKey: StackedService.navigatorKey,
          onGenerateRoute: StackedRouter().onGenerateRoute,
        ));
  }
}

/// This class is used to build page transitions that don't have any animation
/*class _InanimatePageTransitionsBuilder extends PageTransitionsBuilder {
  const _InanimatePageTransitionsBuilder();

  @override
  Widget buildTransitions<T>(
      PageRoute<T> route,
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    return child;
  }
}*/
