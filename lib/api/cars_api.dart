import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/car_model.dart';

import 'api_base.dart';

class CarsApi extends ApiBase {
  CarsApi() : super('cars_api');
  final log = getLogger('CarsApi');
  String commandName = 'car';

  Future<List<CarModel>?> getItems() async {
    log.v('cars getItems');
    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == commandName &&
              data['cars'] != null) {
            List<CarModel> res = [];
            for (var item in data['cars']) {
              CarModel temp = CarModel.fromJson(item);
              res.add(temp);
            }

            return res;
          }
        }
      }
    }

    return null;
  }

  Map<String, dynamic> _getMsg(
      int fleetId,
      String licensePlate,
      String make,
      String model,
      int year,
      String color,
      String starLinkID,
      int zoneId,
      List<String>? driverId) {
    Map<String, dynamic> res = {
      cmdKey: "create_car",
      "device_id": starLinkID,
      "project_id": fleetId.toString(),
      "time_zone": zoneId.toString(),
      "car_number": licensePlate,
      "car_type": make,
      "sub_type": model,
      "car_year": year.toString(),
      "car_color": color,
      // "add_drivers": driverIdStr
    };

    if (driverId != null && driverId.isNotEmpty) {
      if (driverId.length == 1) {
        res["add_drivers"] = driverId[0];
        // res.addEntries(add);
      } else {
        res["add_drivers"] = driverId;
      }
    }

    return res;
  }

  Future<CarModel?> addItem(
      int fleetId,
      String licensePlate,
      String make,
      String model,
      int year,
      String color,
      String starLinkID,
      int zoneId,
      List<String>? driverId) async {
    log.v('car add Item');
    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, dynamic>> formData = [
        sessionId,
        _getMsg(fleetId, licensePlate, make, model, year, color, starLinkID,
            zoneId, driverId)
      ];

      List<dynamic>? res = await sendData(formData);

      if (res != null && res.length > 0) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == "create_car" &&
              data.containsKey('car_id') &&
              data['car_id'] != null &&
              isNumeric(data['car_id'])) {
            CarModel res = CarModel(
                carId: int.parse(data['car_id']),
                deviceId: starLinkID,
                projectId: fleetId,
                timeZone: zoneId,
                carNumber: licensePlate,
                carType: make,
                subType: model,
                carYear: year.toString(),
                carColor: color,
                simImsi: '',
                simPhone: '',
                driverIds: []);

            return res;
          }
        }
      }
    }
    return null;
  }
}
