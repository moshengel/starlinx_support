import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/driver_model.dart';

import 'api_base.dart';

class DriversApi extends ApiBase {
  DriversApi() : super('drivers_api');
  final log = getLogger('DriversApi');
  String commandName = 'driver';

  List<DriverModel> _items = [];
  bool _isLoading = false;
  _setItems(List<DriverModel> items) {
    _items = items;
    _isLoading = true;
  }

  List<DriverModel> getDriversName(List<int> data) {
    List<DriverModel> res = [];
    if (data.isNotEmpty && _items.isNotEmpty) {
      for (int driverId in data) {
        for (DriverModel item in _items) {
          if (item.driverId == driverId) {
            res.add(item);
          }
        }
      }
    }

    return res;
  }

  Future<List<DriverModel>?> getItems(bool force) async {
    log.v('driver getItems');
    if (!force && _isLoading) {
      return _items;
    }

    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == commandName &&
              data['drivers'] != null) {
            List<DriverModel> res = [];
            for (var item in data['drivers']) {
              DriverModel temp = DriverModel.fromJson(item);
              res.add(temp);
            }
            _setItems(res);

            return res;
          }
        }
      }
    }

    _setItems([]);

    return null;
  }

  Future<DriverModel?> addItem(String firstName, String lastName,
      String phoneNumber, int fleetId) async {
    log.v('driver add Item');
    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {
          cmdKey: "create_driver",
          "project_id": fleetId.toString(),
          "first_name": firstName,
          "last_name": lastName,
          "phone_number": phoneNumber,
        }
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null && res.length > 0) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == "create_driver" &&
              data.containsKey('driver_id') &&
              data['driver_id'] != null &&
              isNumeric(data['driver_id'])) {
            DriverModel res = DriverModel(
                driverId: int.parse(data['driver_id']),
                projectId: fleetId,
                ident: '',
                firstName: firstName,
                lastName: lastName,
                phoneNumber: phoneNumber);
            _items.add(res);

            return res;
          }
        }
      }
    }
    return null;
  }
}
