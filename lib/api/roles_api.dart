import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/roles_model.dart';

import 'api_base.dart';

class RolesApi extends ApiBase {
  RolesApi() : super('roles_api');
  final log = getLogger('RolesApi');
  String commandName = 'roles';
  List<RolesModel> _items = [];
  bool _isLoading = false;
  _setItems(List<RolesModel> items) {
    _items = items;
    _isLoading = true;
  }

  String getRoleNameById(int roleId) {
    if (_items.isNotEmpty) {
      for (RolesModel item in _items) {
        if (item.roleId == roleId) {
          return item.description;
        }
      }
    }

    return roleId.toString();
  }

  Future<List<RolesModel>?> getItems(bool force) async {
    log.v('Roles get Items');
    if (!force && _isLoading) {
      return _items;
    }

    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == commandName &&
              data.containsKey('roles') &&
              data['roles'] != null) {
            List<RolesModel> res = [];
            for (var item in data['roles']) {
              RolesModel temp = RolesModel.fromJson(item);
              res.add(temp);
            }
            _setItems(res);

            return res;
          }
        }
      }
    }

    _setItems([]);

    return null;
  }
}
