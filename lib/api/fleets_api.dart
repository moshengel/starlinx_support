import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/fleet_model.dart';

import 'api_base.dart';

class FleetsApi extends ApiBase {
  FleetsApi() : super('fleets_api');
  final log = getLogger('FleetsApi');
  String commandName = 'project';
  List<FleetModel> _items = [];
  bool _isLoading = false;
  _setItems(List<FleetModel> items) {
    _items = items;
    _isLoading = true;
  }

  String getFleetNameById(int fleetId) {
    if (_items.isNotEmpty) {
      for (FleetModel item in _items) {
        if (item.projectId == fleetId) {
          return item.name;
        }
      }
    }

    return fleetId.toString();
  }

  Future<List<FleetModel>?> getItems(bool force) async {
    log.v('fleets getItems');
    if (!force && _isLoading) {
      return _items;
    }

    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == commandName &&
              data['data'] != null) {
            List<FleetModel> res = [];
            for (var item in data['data']) {
              FleetModel temp = FleetModel.fromJson(item);
              res.add(temp);
            }
            _setItems(res);

            return res;
          }
        }
      }
    }

    _setItems([]);

    return null;
  }

  Future<FleetModel?> addItem(String name) async {
    log.v('fleets add Item');
    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: "create_project", "name": name}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == "create_project" &&
              data.containsKey('project_id') &&
              data['project_id'] != null &&
              isNumeric(data['project_id'])) {
            FleetModel res = FleetModel(
                projectId: int.parse(data['project_id']), name: name);
            _items.add(res);
            return res;
          }
        }
      }
    }

    return null;
  }
}
