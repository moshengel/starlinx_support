import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/time_zone_model.dart';

import 'api_base.dart';

class TimeZoneApi extends ApiBase {
  TimeZoneApi() : super('time_zone_api');
  final log = getLogger('TimeZoneApi');
  String commandName = 'time_zone';
  List<TimeZoneModel> _items = [];
  bool _isLoading = false;
  _setItems(List<TimeZoneModel> items) {
    _items = items;
    _isLoading = true;
  }

  String getZoneNameById(int zoneId) {
    if (_items.isNotEmpty) {
      for (TimeZoneModel item in _items) {
        if (item.zoneId == zoneId) {
          return item.zoneName;
        }
      }
    }

    return zoneId.toString();
  }

  Future<List<TimeZoneModel>?> getItems(bool force) async {
    log.v('Time Zone getItems');
    if (!force && _isLoading) {
      return _items;
    }

    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == commandName &&
              data.containsKey('zones') &&
              data['zones'] != null) {
            List<TimeZoneModel> res = [];
            for (var item in data['zones']) {
              TimeZoneModel temp = TimeZoneModel.fromJson(item);
              res.add(temp);
            }
            _setItems(res);

            return res;
          }
        }
      }
    }

    _setItems([]);

    return null;
  }
}
