import 'dart:convert';

import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/profile_account_model.dart';
import 'package:starlinx_support/providers/auth.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:http/http.dart' as http;
import 'package:starlinx_support/util/app_url.dart';

abstract class ApiBase {
  final log = getLogger('ApiBase');
  UserService _userService = locator<UserService>();
  Uri _baceUrlString = Uri.parse(AppUrl.baseURLString);
  Uri _baceUrl = Uri.parse(AppUrl.baseURL);
  final String cmdKey = 'cmd';
  String commandName;

  ApiBase(this.commandName);
  changeStatusLogin(Status status) {
    _userService.changeStatus(Status.Authenticating);
  }

  setProfile(String session, ProfileAccountModel myProfile) {
    _userService.setProfile(session, myProfile);
  }

  bool hasSessionId() {
    return _userService.hasLoggedInUser;
  }

  setSessionId(String session) {
    _userService.setSessionId(session);
  }

  String? getSessionId() {
    return _userService.currentToken;
  }

  bool checkResSessionIdAPI(Map<String, dynamic> res) {
    if (res['status']) {
      setSessionId(res['message']);
    } else {
      _userService.logout(res['message'], true);
    }

    return res['status'];
  }

  Map<String, String>? bindMsgSessionIdAPI() {
    if (hasSessionId()) {
      String? sessionId = getSessionId();
      return {
        "cmd": "login",
        "session_id": (sessionId != null ? sessionId : '')
      };
    }

    return null;
  }

  Future<Map<String, dynamic>?> sendDataString(String data) async {
    try {
      http.Response response = await http.post(
        _baceUrlString,
        body: data,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
          // 'Content-Type': 'application/json; charset=utf-8'
        },
      );
      if (response.statusCode == 200 && response.body.isNotEmpty) {
        return jsonDecode(response.body);
      }
    } catch (e) {
      log.e('Error:$e');
    }

    return null;
  }

  Map<String, dynamic> checkIsAuthAPI(Map<String, dynamic> responseData) {
    Map<String, dynamic> result = {'status': false, 'message': 'error'};
    if (responseData.containsKey(cmdKey) && responseData[cmdKey] == 'login') {
      if (responseData['error'] != null) {
        result['message'] = responseData['error'].toString();

        // userService.logout(responseData['error'].toString());
      } else if (responseData['session_id'] != null &&
          responseData['session_id'].toString().length > 0) {
        result['status'] = true;
        result['message'] = responseData['session_id'].toString();

        // userService.setSessionId(responseData['session_id'].toString());
      }
    }

    return result;
  }

  Future<List<dynamic>?> sendData(List<Map<String, dynamic>> data) async {
    try {
      http.Response response = await http.post(
        _baceUrl,
        // body: formDataString,
        body: json.encode(data),
        headers: {'Content-Type': 'application/json'},
      );
      if (response.statusCode == 200 && response.body.isNotEmpty) {
        final List<dynamic> responseData = json.decode(response.body);
        if (responseData.length > 0) {
          return responseData;
        }
      }
    } catch (e) {
      return null;
    }
    return null;
  }

  bool isNumeric(String? s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  /*bool checkIsAuth(Map<String, dynamic> responseData) {
    if (responseData['cmd'] == 'login') {
      if (responseData['error'] != null) {
        userService.logout(responseData['error'].toString());
      } else if (responseData['session_id'] != null &&
          responseData['session_id'].toString().length > 0) {
        userService.setSessionId(responseData['session_id'].toString());

        return true;
      }
    }

    return false;
  }
  Future<Map<String, dynamic>?> sendData(
      List<Map<String, dynamic>> data) async {
    http.Response response = await http.post(
      _baceUrl,
      // body: formDataString,
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      final List<dynamic> responseData = json.decode(response.body);
      if (responseData.length > 0) {
        if (checkIsAuth(responseData[0]) && responseData.length > 1) {
          return responseData[1];
        }
      }
    } else {
      return null;
    }
  }
*/
}
