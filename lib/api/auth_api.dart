import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/profile_account_model.dart';
import 'package:starlinx_support/providers/auth.dart';

import 'api_base.dart';

class AuthApi extends ApiBase {
  AuthApi() : super('auth_api');
  final log = getLogger('AuthApi');

  Future<void> login(String username, String password) async {
    changeStatusLogin(Status.Authenticating);
    String formDataString = 'qL=1&Debug=1&qT=Login&u=$username&p=$password';

    Map<String, dynamic>? res = await sendDataString(formDataString);
    if (res != null && res.isNotEmpty) {
      if (res.containsKey('Session') && res['Session'].toString().isNotEmpty) {
        String sessionId = res['Session'].toString();
        ProfileAccountModel myProfile = ProfileAccountModel.fromJson(res);

        setProfile(sessionId, myProfile);
      }
      /*if (res.containsKey('Title') && res['Title'] == 'Error') {
        userService.changeStatus(Status.LoggedOut);
      } else {}*/
      // Map<String, dynamic> session = checkIsAuth(res[0]);
    } else {
      changeStatusLogin(Status.LoggedOut);
    }
  }

  /*Future<User?> getUserAccount(String username) async {
    log.v('Sync user $username');
    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName, "user_name": username}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data['users'] != null && data['users'][0] != null) {
            return User.fromJson(data['users'][0]);
          }
        }
      }
    }

    return null;
  }*/
}
