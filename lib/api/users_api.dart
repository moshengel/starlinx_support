import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/user_model.dart';

import 'api_base.dart';

class UsersApi extends ApiBase {
  UsersApi() : super('users_api');
  final log = getLogger('UsersApi');
  String commandName = 'user';

  Future<List<UserModel>?> getItems() async {
    log.v('users getItems');
    Map<String, String>? sessionId = bindMsgSessionIdAPI();
    if (sessionId != null) {
      final List<Map<String, String>> formData = [
        sessionId,
        {cmdKey: commandName}
      ];

      List<dynamic>? res = await sendData(formData);
      if (res != null) {
        Map<String, dynamic> session = checkIsAuthAPI(res[0]);
        if (checkResSessionIdAPI(session) && res.length > 1) {
          Map<String, dynamic> data = res[1];
          if (data.containsKey(cmdKey) &&
              data[cmdKey] == commandName &&
              data['users'] != null) {
            List<UserModel> res = [];
            for (var item in data['users']) {
              UserModel temp = UserModel.fromJson(item);
              res.add(temp);
            }

            return res;
          }
        }
      }
    }

    return null;
  }
}
