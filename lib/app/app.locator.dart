// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedLocatorGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:starlinx_support/api/cars_api.dart';
import 'package:starlinx_support/api/drivers_api.dart';
import 'package:starlinx_support/api/auth_api.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/api/roles_api.dart';
import 'package:starlinx_support/api/time_zone_api.dart';
import 'package:starlinx_support/api/users_api.dart';
import 'package:starlinx_support/services/shared_preference.dart';
import 'package:starlinx_support/services/user_service.dart';

final locator = StackedLocator.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => AppPreferencesService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => AuthApi());
  locator.registerLazySingleton(() => UsersApi());
  locator.registerLazySingleton(() => DriversApi());
  locator.registerLazySingleton(() => FleetsApi());
  locator.registerLazySingleton(() => CarsApi());
  locator.registerLazySingleton(() => TimeZoneApi());
  locator.registerLazySingleton(() => RolesApi());

  // locator.registerLazySingleton(() => PlacesService());
  locator.registerLazySingleton(() => DialogService());
  //locator.registerLazySingleton(() => EnvironmentService());
  //locator.registerSingleton(FirebaseAuthenticationService());
}
