import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:starlinx_support/ui/home/home_view.dart';
import 'package:starlinx_support/ui/login/login.dart';
import 'package:starlinx_support/ui/startup/startup_view.dart';

class PageTitles {
  static const String homeView = 'Home';
  static const String loginView = 'Login';
  // static const String usersView = 'Users';
  // static const String fleetsView = 'Fleets';
  // static const String carsView = 'Cars';
  // static const String driversView = 'Drivers';
  // static const String settingsView = 'Settings';
  static const all = <String>{
    homeView,
    loginView,
    // usersView,
    // fleetsView,
    // carsView,
    // driversView,
    // settingsView
  };
}

class Routes {
  static const String startUpView = '/';
  static const String loginView = '/login';
  static const String homeView = '/home';
  // static const String usersView = '/users';
  // static const String fleetsView = '/fleets';
  // static const String carsView = '/cars';
  // static const String driversView = '/drivers';
  // static const String settings = '/settings';
  static const all = <String>{
    startUpView,
    loginView,
    homeView,
    // usersView,
    // fleetsView,
    // carsView,
    // driversView,
    // settings
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.startUpView, page: StartUpView),
    RouteDef(Routes.loginView, page: Login),
    RouteDef(Routes.homeView, page: HomeView),
    // RouteDef(Routes.settings, page: SettingsView),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    StartUpView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const StartUpView(),
        settings: data,
      );
    },
    Login: (data) {
      /*var args = data.getArgs<LoginViewArguments>(
        orElse: () => LoginViewArguments(),
      );*/
      return MaterialPageRoute<dynamic>(
        builder: (context) => Login(),
        settings: data,
      );
    },
    HomeView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomeView(),
        settings: data,
      );
    },
    /*SettingsView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const SettingsView(),
        settings: data,
      );
    },*/
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// LoginView arguments holder class
/*class LoginViewArguments {
  final Key? key;
  LoginViewArguments({this.key});
}*/
