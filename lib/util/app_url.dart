class AppUrl {
  static const String baseURLString = "https://karmadev.erm.co.il/i/";
  //static const String baseURLString = "https://karma.online/i/";

  static const String baseURL = "https://karmadev.erm.co.il/i/api/";
  // static const String baseURL = "https://karma.online/api/";

  // static const String localBaseURL = "http://10.0.2.2:4000/api/v1";

  // static const String baseURL = liveBaseURL;

  // static const String login = baseURL + "/session";
  // static const String register = baseURL + "/registration";
  // static const String forgotPassword = baseURL + "/forgot-password";
}
