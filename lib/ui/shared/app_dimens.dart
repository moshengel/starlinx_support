const double mdRadiusCircular = 10.0;
const double mdDefaultPadding = 20.0;
const double mdMaxWidth = 1932.0;
const Duration kDefaultDuration = Duration(milliseconds: 250);
