import 'package:flutter/material.dart';

// const Color kcPrimaryColor = Color(0xff22A45D);

const Color mcBackgroundColor = Color(0xFF26262A);
const Color mcBackgroundColorWhite = Color(0xFFFFFFFF);

const Color mcBackgroundColorOpacity = Color(0xFF424246);

const Color mcPrimaryColor = Color(0xFFB2D234);
const Color mcAccentColor = Color(0xFFB2D234);
const Color mcTextColor = Color(0xFFEBEBEB);
const Color mcTextColorDark = Color(0xFF424246);

const Color kcMediumGreyColor = Color(0xff868686);
const Color kcLightGreyColor = Color(0xffe5e5e5);
const Color kcVeryLightGreyColor = Color(0xfff2f2f2);

const kDarkBlackColor = Color(0xFF191919);

const MaterialColor mcPrimarySwatch = MaterialColor(
  0xFFB2D234,
  <int, Color>{
    50: mcPrimaryColor,
    100: mcPrimaryColor,
    200: mcPrimaryColor,
    300: mcPrimaryColor,
    400: mcPrimaryColor,
    500: mcPrimaryColor,
    600: mcPrimaryColor,
    700: mcPrimaryColor,
    800: mcPrimaryColor,
    900: mcPrimaryColor,
  },
);
