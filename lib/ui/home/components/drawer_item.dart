import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';

class DrawerItem extends StatefulWidget {
  final IconData? icon;
  final String text;
  final bool isActive;
  final bool isHeader;
  final VoidCallback press;

  const DrawerItem({
    Key? key,
    required this.text,
    required this.icon,
    required this.isActive,
    required this.isHeader,
    required this.press,
  }) : super(key: key);

  @override
  _MenuItemState createState() => _MenuItemState();
}

class _MenuItemState extends State<DrawerItem> {
  bool _isHover = false;

  Color _borderColor() {
    if (widget.isActive) {
      return mcPrimaryColor;
    } else if (!widget.isActive & _isHover) {
      return mcPrimaryColor.withOpacity(0.4);
    }

    return Colors.transparent;
  }

  Color _textColor() {
    if (widget.isActive) {
      return mcPrimaryColor;
    } else if (!widget.isActive & _isHover) {
      return mcPrimaryColor.withOpacity(0.4);
    }

    return mcTextColor;
  }

  BoxDecoration _boxDecoration() {
    if (widget.isHeader) {
      return BoxDecoration(
        border: Border(
          bottom: BorderSide(color: _borderColor(), width: 3),
        ),
      );
    } else {
      return BoxDecoration(
        border: Border(
          right: BorderSide(color: _borderColor(), width: 3),
        ),
      );
    }
  }

  EdgeInsets _padding() {
    if (widget.isHeader) {
      return EdgeInsets.symmetric(vertical: mdDefaultPadding / 2);
    } else {
      return EdgeInsets.symmetric(
          horizontal: mdDefaultPadding, vertical: mdDefaultPadding / 2);
    }
  }

  EdgeInsets _margin() {
    if (widget.isHeader) {
      return EdgeInsets.symmetric(horizontal: mdDefaultPadding);
    } else {
      return EdgeInsets.symmetric();
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.press,
      onHover: (value) {
        setState(() {
          _isHover = value;
        });
      },
      child: AnimatedContainer(
        duration: kDefaultDuration,
        margin: _margin(),
        padding: _padding(),
        decoration: _boxDecoration(),
        child: Column(
          children: [
            Row(
              children: [
                if (!widget.isHeader && widget.icon != null)
                  Icon(widget.icon, color: _textColor()),
                if (!widget.isHeader && widget.icon != null)
                  SizedBox(width: mdDefaultPadding),
                Text(
                  widget.text,
                  style: TextStyle(
                    color: _textColor(),
                    fontWeight:
                        widget.isActive ? FontWeight.w600 : FontWeight.normal,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
