import 'package:flutter/material.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/util/responsive.dart';

import 'datatable_header.dart';

class ResponsiveDatatable extends StatefulWidget {
  final List<DatatableHeader>? headers;
  final List<Map<String, dynamic>>? source;
  final String title;
  final Function(dynamic value)? onSort;
  final String? sortColumn;
  final bool sortAscending;

  final List<Widget>? actions;
  final List<Widget>? footers;
  final Function(dynamic value)? onTabRow;

  final bool isLoading;
  final bool isExpandRows;
  final List<bool>? expanded;
  final String mainField;
  const ResponsiveDatatable(
      {Key? key,
      this.headers,
      this.source,
      required this.title,
      this.onSort,
      this.sortColumn,
      this.sortAscending = false,
      this.onTabRow,
      this.actions,
      this.footers,
      this.isLoading = false,
      this.isExpandRows = true,
      this.expanded,
      required this.mainField})
      : super(key: key);

  @override
  _ResponsiveDatatableState createState() => _ResponsiveDatatableState();
}

class _ResponsiveDatatableState extends State<ResponsiveDatatable> {
  bool _isMobileState(BuildContext context) {
    return !Responsive.isDesktop(context);
  }

  Alignment _headerAlignSwitch(TextAlign textAlign) {
    switch (textAlign) {
      case TextAlign.center:
        return Alignment.center;
      case TextAlign.left:
        return Alignment.centerLeft;
      case TextAlign.right:
        return Alignment.centerRight;
      default:
        return Alignment.center;
    }
  }

  Widget _getTitle() {
    return Text(widget.title, style: const TextStyle(fontSize: 18.0));
  }

  Widget _getCardHeader(bool isMobile) {
    return Container(
      height: !isMobile ? 60 : null,
      padding: isMobile
          ? const EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 8.0)
          : const EdgeInsets.fromLTRB(18.0, 12.0, 18.0, 12.0),
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: mcAccentColor))),
      child: isMobile
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _getTitle(),
                    if (widget.onSort != null)
                      PopupMenuButton(
                          child: Container(
                            padding: const EdgeInsets.all(15),
                            child: const Text("SORT BY"),
                          ),
                          tooltip: "SORT BY",
                          initialValue: widget.sortColumn,
                          itemBuilder: (_) => widget.headers!
                              .where((header) =>
                                  header.show == true &&
                                  header.sortable == true)
                              .toList()
                              .map((header) => PopupMenuItem(
                                    child: Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: [
                                        Text(
                                          header.text.toUpperCase(),
                                          textAlign: header.textAlign,
                                        ),
                                        if (widget.sortColumn != null &&
                                            widget.sortColumn == header.value)
                                          widget.sortAscending
                                              ? const Icon(Icons.arrow_downward,
                                                  size: 15)
                                              : const Icon(Icons.arrow_upward,
                                                  size: 15)
                                      ],
                                    ),
                                    value: header.value,
                                  ))
                              .toList(),
                          onSelected: (value) {
                            if (widget.onSort != null) widget.onSort!(value);
                          })
                  ],
                ),
                if (widget.actions != null) const SizedBox(height: 8.0),
                if (widget.actions != null)
                  Container(
                    alignment: Alignment.center,
                    height: 40,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [...?widget.actions]),
                  ),
              ],
            )
          : Row(
              children: [
                _getTitle(),
                if (widget.actions != null) const SizedBox(width: 12.0),
                if (widget.actions != null)
                  Expanded(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [...?widget.actions]))
                // if (widget.actions != null) ...?widget.actions
              ],
            ),
    );
  }

  Widget _desktopHeader() {
    return Container(
      padding: const EdgeInsets.all(11.0),
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 1))),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ...widget.headers!
              .where((DatatableHeader header) => header.show == true)
              .map(
                (DatatableHeader header) => Expanded(
                    flex: header.flex ?? 1,
                    child: InkWell(
                      onTap: () {
                        if (widget.onSort != null && header.sortable) {
                          widget.onSort!(header.value);
                        }
                      },
                      child: header.headerBuilder != null
                          ? header.headerBuilder!(header.value)
                          : Container(
                              padding: const EdgeInsets.all(11),
                              alignment: _headerAlignSwitch(header.textAlign),
                              child: Wrap(
                                crossAxisAlignment: WrapCrossAlignment.center,
                                children: [
                                  Text(
                                    header.text.toUpperCase(),
                                    textAlign: header.textAlign,
                                  ),
                                  if (widget.sortColumn != null &&
                                      widget.sortColumn == header.value)
                                    widget.sortAscending
                                        ? const Icon(Icons.arrow_downward,
                                            size: 15)
                                        : const Icon(Icons.arrow_upward,
                                            size: 15)
                                ],
                              ),
                            ),
                    )),
              )
              .toList()
        ],
      ),
    );
  }

  List<Widget> _desktopList() {
    List<Widget> widgets = [];
    for (var index = 0; index < widget.source!.length; index++) {
      final data = widget.source![index];
      widgets.add(Column(
        children: [
          InkWell(
            onTap: widget.onTabRow != null
                ? () {
                    widget.onTabRow!(data);
                  }
                : null,
            child: Container(
              padding: const EdgeInsets.all(11.0),
              decoration: const BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: Colors.grey, width: 1))),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ...widget.headers!
                      .where((DatatableHeader header) => header.show == true)
                      .map(
                        (DatatableHeader header) => Expanded(
                          flex: header.flex ?? 1,
                          child: header.sourceBuilder != null
                              ? header.sourceBuilder!(data[header.value], data)
                              : Container(
                                  padding: const EdgeInsets.all(11),
                                  child: Text(
                                    "${data[header.value]}",
                                    textAlign: header.textAlign,
                                  ),
                                ),
                        ),
                      )
                      .toList()
                ],
              ),
            ),
          ),
        ],
      ));
    }
    return widgets;
  }

  List<Widget> _mobileList() {
    List<Widget> widgets = [];
    for (var index = 0; index < widget.source!.length; index++) {
      final data = widget.source![index];
      widgets.add(Column(
        children: [
          InkWell(
            onTap: () {
              if (widget.onTabRow != null && widget.expanded != null) {
                widget.onTabRow!(data);

                setState(() {
                  widget.expanded![index] = !widget.expanded![index];
                });
              }
            },
            child: Container(
              padding: const EdgeInsets.all(13.0),
              decoration: const BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: Colors.grey, width: 1))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(data[widget.mainField]),
                  if (widget.isExpandRows &&
                      widget.expanded != null &&
                      widget.expanded![index])
                    const Icon(Icons.arrow_drop_down_outlined),
                  if (!widget.isExpandRows ||
                      widget.expanded == null ||
                      !widget.expanded![index])
                    const Icon(Icons.arrow_right_outlined),
                ],
              ),
            ),
          ),
          if (widget.isExpandRows && widget.expanded![index])
            Container(
              padding: const EdgeInsets.all(13.0),
              decoration: const BoxDecoration(
                  color: mcBackgroundColorOpacity,
                  border:
                      Border(bottom: BorderSide(color: Colors.grey, width: 1))),
              child: Column(
                children: [
                  ...widget.headers!
                      .where((header) => header.show == true)
                      .toList()
                      .map(
                        (header) => Container(
                          padding: const EdgeInsets.all(11),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              header.headerBuilder != null
                                  ? header.headerBuilder!(header.value)
                                  : Text(
                                      header.text,
                                      overflow: TextOverflow.clip,
                                    ),
                              const Spacer(),
                              header.sourceBuilder != null
                                  ? header.sourceBuilder!(
                                      data[header.value], data)
                                  : Text("${data[header.value]}")
                            ],
                          ),
                        ),
                      )
                      .toList()
                ],
              ),
            ),
        ],
      ));
    }
    return widgets;
  }

  Widget _getFooters(bool isMobile) {
    if (isMobile) {
      return Container(
        alignment: Alignment.center,
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [...?widget.footers],
        ),
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [...?widget.footers],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    bool _isMobile = _isMobileState(context);
    return Container(
      alignment: Alignment.center,
      child: Column(
        children: [
          //title and actions
          _getCardHeader(_isMobile),

          //desktopHeader
          if (!_isMobile &&
              widget.headers != null &&
              widget.headers!.isNotEmpty)
            _desktopHeader(),
          if (widget.isLoading) const LinearProgressIndicator(),
          // desktopList
          Expanded(
              child: (widget.source != null && widget.source!.isNotEmpty)
                  ? ListView(
                      children: !_isMobile ? _desktopList() : _mobileList())
                  : const SizedBox()),

          //footer
          if (widget.footers != null) _getFooters(_isMobile)
        ],
      ),
    );
  }
}
