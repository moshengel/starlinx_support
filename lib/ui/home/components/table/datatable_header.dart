import 'package:flutter/material.dart';

class DatatableHeader {
  final String text;
  final String value;
  final bool sortable;
  bool show;
  final TextAlign textAlign;
  final int? flex;
  final Widget Function(dynamic value)? headerBuilder;
  final Widget Function(dynamic value, Map<String, dynamic> row)? sourceBuilder;

  DatatableHeader({
    required this.text,
    this.textAlign = TextAlign.center,
    required this.sortable,
    required this.value,
    this.show = true,
    this.flex,
    this.headerBuilder,
    this.sourceBuilder,
  });

  factory DatatableHeader.fromMap(Map<String, dynamic> map) => DatatableHeader(
        text: map['text'],
        value: map['value'],
        sortable: map['sortable'],
        show: map['show'],
        textAlign: map['textAlign'],
        flex: map['flex'],
        headerBuilder: map['headerBuilder'],
        sourceBuilder: map['sourceBuilder'],
      );
  Map<String, dynamic> toMap() => {
        "text": text,
        "value": value,
        "sortable": sortable,
        "show": show,
        "textAlign": textAlign,
        "flex": flex,
        "headerBuilder": headerBuilder,
        "sourceBuilder": sourceBuilder,
      };
}
