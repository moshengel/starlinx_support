import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/ui/shared/ui_helpers.dart';

class QRViewComponent extends StatefulWidget {
  final ValueChanged<String> onChanged;

  const QRViewComponent({required this.onChanged});

  @override
  State<StatefulWidget> createState() => _QRViewComponentState();
}

class _QRViewComponentState extends State<QRViewComponent> {
  Barcode? result;
  String? _code;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  @override
  Widget build(BuildContext context) {
    // double _screenWidth = screenWidth(context);
    //var size = _getQrSize(context);

    return Stack(
      children: <Widget>[
        SingleChildScrollView(
            child: Container(
          width: 600,
          // width: _screenWidth < 500.0 ? _screenWidth : 500.0,
          // height: 400,
          padding: EdgeInsets.all(8),
          // margin: EdgeInsets.only(top: mdDefaultPadding),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(mdRadiusCircular),
              boxShadow: [
                BoxShadow(
                    // color: color.withOpacity(0.8),
                    color: Colors.black,
                    offset: Offset(1.0, 2.0),
                    blurRadius: mdRadiusCircular),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 600,
                height: screenHeight(context) - 200,
                child: _buildQrView(context),
              ),
              verticalSpaceRegular,

              if (_code != null && _code!.isNotEmpty)
                Text(_code!, style: TextStyle(color: mcTextColorDark))
              //'Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}')
              else
                Text('Scan a code', style: TextStyle(color: mcTextColorDark)),
              verticalSpaceRegular,
              ElevatedButton(
                  onPressed: () async {
                    await controller?.toggleFlash();
                    setState(() {});
                  },
                  child: FutureBuilder(
                    future: controller?.getFlashStatus(),
                    builder: (context, snapshot) {
                      // String text = (snapshot.data == true) ? 'YES' : 'NO';
                      // return Text('Flash: $text');
                      IconData _icon = (snapshot.data == true)
                          ? Icons.flash_on_outlined
                          : Icons.flash_off_outlined;
                      return Icon(_icon);
                    },
                  ))

              //_buildQrView(context),
            ],
          ),
        ))
      ],
    );
  }

  /*@override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(flex: 1, child: _buildQrView(context)),
          Expanded(
            flex: 1,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  if (result != null)
                    Text(
                        'Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}')
                  else
                    Text('Scan a code'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                            onPressed: () async {
                              await controller?.toggleFlash();
                              setState(() {});
                            },
                            child: FutureBuilder(
                              future: controller?.getFlashStatus(),
                              builder: (context, snapshot) {
                                return Text('Flash: ${snapshot.data}');
                              },
                            )),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                            onPressed: () async {
                              await controller?.flipCamera();
                              setState(() {});
                            },
                            child: FutureBuilder(
                              future: controller?.getCameraInfo(),
                              builder: (context, snapshot) {
                                if (snapshot.data != null) {
                                  return Text(
                                      'Camera facing ${describeEnum(snapshot.data!)}');
                                } else {
                                  return Text('loading');
                                }
                              },
                            )),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          onPressed: () async {
                            await controller?.pauseCamera();
                          },
                          child: Text('pause', style: TextStyle(fontSize: 20)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          onPressed: () async {
                            await controller?.resumeCamera();
                          },
                          child: Text('resume', style: TextStyle(fontSize: 20)),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }*/
  double _getQrSize(BuildContext context) {
    double _screenWidth = screenWidth(context);
    double _screenHeight = screenHeight(context);
    double size = (_screenWidth > _screenHeight) ? _screenHeight : _screenWidth;
    return (size > 500) ? 500 : size;
  }

  Color _borderColor = Colors.red;
  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    double size = _getQrSize(context);
    var scanArea = (size < 400) ? 200.0 : 300.0;
    //double scanArea = size - 150.0;

    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          overlayColor: Colors.white.withOpacity(0.8),
          // overlayColor: Colors.white,
          borderColor: _borderColor,
          borderRadius: mdRadiusCircular,
          borderLength: 30,
          borderWidth: 6.0,
          cutOutBottomOffset: 0,
          cutOutSize: scanArea),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      String codeIcome = scanData.code;
      Color temp = Colors.red;
      String code = '';
      // String codeIcome = '11:22:33:AA:BB:CC';

      if (codeIcome.length == 17) {
        temp = mcPrimaryColor;
        code = codeIcome;
      }

      setState(() {
        _borderColor = temp;
        _code = code;
      });

      widget.onChanged(_code!);
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
