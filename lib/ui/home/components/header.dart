import 'package:flutter/material.dart';
import 'package:starlinx_support/controllers/MenuController.dart';
import 'package:starlinx_support/ui/home/components/socal.dart';
import 'package:starlinx_support/ui/home/components/web_menu.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/util/responsive.dart';

class Header extends StatelessWidget implements PreferredSizeWidget {
  Header(this._controller, {Key? key}) : super(key: key);

  final MenuController _controller;
  // GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  Size get preferredSize => const Size.fromHeight(80);

  @override
  Widget build(BuildContext context) {
    //MenuController _controller = Get.put(MenuController());

    final menuIconField = IconButton(
      padding: EdgeInsets.fromLTRB(0.0, 0.0, mdDefaultPadding, 0.0),
      icon: Icon(
        Icons.menu_outlined,
        // color: Colors.white,
      ),
      onPressed: () {
        _controller.openOrCloseDrawer();
      },
    );
    final logoField = Container(
      width: 120,
      // height: 40,
      child: Image.asset("asset/images/logo.png"),
    );
    EdgeInsets _getPadding() {
      if (Responsive.isMobile(context)) {
        return EdgeInsets.fromLTRB(
            mdDefaultPadding, 16.0, mdDefaultPadding, 16.0);
      }

      return EdgeInsets.all(mdDefaultPadding);
    }

    return Container(
      width: double.infinity,
      color: kDarkBlackColor,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: mdMaxWidth),
              // padding: EdgeInsets.all(mdDefaultPadding),
              padding: _getPadding(),

              child: Column(
                children: [
                  Row(
                    children: [
                      if (!Responsive.isDesktop(context)) menuIconField,
                      logoField,
                      SizedBox(width: mdDefaultPadding),

                      // Spacer(),
                      if (Responsive.isDesktop(context)) WebMenu(),
                      Spacer(),
                      // Socal
                      Socal(),
                    ],
                  ),
                  /*SizedBox(height: kDefaultPadding * 2),
                  Text(
                    "Welcome to Our Blog",
                    style: TextStyle(
                      fontSize: 32,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: kDefaultPadding),
                    child: Text(
                      "Stay updated with the newest design and development stories, case studies, \nand insights shared by DesignDK Team.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Raleway',
                        height: 1.5,
                      ),
                    ),
                  ),
                  FittedBox(
                    child: TextButton(
                      onPressed: () {},
                      child: Row(
                        children: [
                          Text(
                            "Learn More",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: kDefaultPadding / 2),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  */
                  /*if (Responsive.isDesktop(context))
                    SizedBox(height: mdDefaultPadding),*/
                ],
              ),
            )
          ],
        ),
      ),
    );

    /*return Scaffold(
        body: Container(
      width: double.infinity,
      color: kDarkBlackColor,
      height: 160.0,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: mdMaxWidth),
              padding: EdgeInsets.all(mdDefaultPadding),
              child: Column(
                children: [
                  Row(
                    children: [
                      if (!Responsive.isDesktop(context)) menuIconField,
                      logoField,
                      Spacer(),
                      if (Responsive.isDesktop(context)) WebMenu(),
                      Spacer(),
                      // Socal
                      Socal(),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ));*/
    /*return Container(
      key: _scaffoldKey,
      width: double.infinity,
      color: kDarkBlackColor,
      child: SafeArea(
        child: Column(
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: mdMaxWidth),
              padding: EdgeInsets.all(mdDefaultPadding),
              child: Column(
                children: [
                  Row(
                    children: [
                      if (!Responsive.isDesktop(context)) menuIconField,
                      logoField,
                      Spacer(),
                      if (Responsive.isDesktop(context)) WebMenu(),
                      Spacer(),
                      // Socal
                      Socal(),
                    ],
                  ),
                  /*SizedBox(height: kDefaultPadding * 2),
                  Text(
                    "Welcome to Our Blog",
                    style: TextStyle(
                      fontSize: 32,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: kDefaultPadding),
                    child: Text(
                      "Stay updated with the newest design and development stories, case studies, \nand insights shared by DesignDK Team.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Raleway',
                        height: 1.5,
                      ),
                    ),
                  ),
                  FittedBox(
                    child: TextButton(
                      onPressed: () {},
                      child: Row(
                        children: [
                          Text(
                            "Learn More",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(width: kDefaultPadding / 2),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  */
                  /*if (Responsive.isDesktop(context))
                    SizedBox(height: mdDefaultPadding),*/
                ],
              ),
            )
          ],
        ),
      ),
    );*/
  }
}
