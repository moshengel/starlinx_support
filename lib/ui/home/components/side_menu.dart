import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/controllers/MenuController.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/home/components/drawer_item.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/util/responsive.dart';

class SideMenu extends StatelessWidget {
  SideMenu(this._controller, {Key? key}) : super(key: key);

  final MenuController _controller;

  @override
  Widget build(BuildContext context) {
    // MenuController _controller = Get.put(MenuController());

    final _userService = locator<UserService>();

    VoidCallback doLogout = () {
      _userService.logout(null, true);
    };
    //return Drawer(
    return Container(
      width: 220,
      color: kDarkBlackColor,
      child: Obx(
        () => ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: mdDefaultPadding),
                child: Image.asset("asset/images/logo.png"),
              ),
            ),
            ...List.generate(
              _controller.menuItems.length,
              (index) => DrawerItem(
                text: _controller.menuItems[index].title.toUpperCase(),
                icon: _controller.menuItems[index].icon,
                isActive: index == _controller.selectedIndex,
                isHeader: false,
                press: () {
                  _controller.setMenuIndex(context, index);
                },
              ),
            ),
            if (Responsive.isMobile(context)) const Divider(),
            if (Responsive.isMobile(context))
              DrawerItem(
                text: "LOGOUT",
                icon: Icons.logout_outlined,
                isActive: false,
                isHeader: false,
                press: doLogout,
              ),
            // elevatedButtons(context, "LOGOUT", doLogin),
          ],
        ),
      ),
    );
    //);
  }
}
