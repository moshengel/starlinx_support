import 'package:flutter/material.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/ui/widgets/widgets.dart';
import 'package:starlinx_support/util/responsive.dart';

class Socal extends StatelessWidget {
  const Socal({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _userService = locator<UserService>();
    final name = _userService.currentUser != null
        ? _userService.currentUser!.firstName
        : "username";
    VoidCallback doLogout = () {
      _userService.logout(null, true);
    };
    return Row(
      children: [
        label(name),
        if (!Responsive.isMobile(context)) SizedBox(width: mdDefaultPadding),
        if (!Responsive.isMobile(context))
          elevatedButtons(context, "LOGOUT", doLogout),

        /* if (!Responsive.isMobile(context))
          SvgPicture.asset("assets/icons/behance-alt.svg"),
        if (!Responsive.isMobile(context))
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: mdDefaultPadding / 2),
            child: SvgPicture.asset("assets/icons/feather_dribbble.svg"),
          ),
        if (!Responsive.isMobile(context))
          SvgPicture.asset("assets/icons/feather_twitter.svg"),*/
        // SizedBox(width: mdDefaultPadding),
        /*ElevatedButton(
          onPressed: () {},
          style: TextButton.styleFrom(
            padding: EdgeInsets.symmetric(
              horizontal: mdDefaultPadding * 1.5,
              vertical:
                  mdDefaultPadding / (Responsive.isDesktop(context) ? 1 : 2),
            ),
          ),
          child: Text("Let's Talk"),
        ),*/
      ],
    );
  }
}
