import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:starlinx_support/controllers/MenuController.dart';

import 'drawer_item.dart';

class WebMenu extends StatelessWidget {
  final MenuController _controller = Get.put(MenuController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        children: List.generate(
          _controller.menuItems.length,
          (index) => DrawerItem(
            text: _controller.menuItems[index].title.toUpperCase(),
            icon: _controller.menuItems[index].icon,
            isActive: index == _controller.selectedIndex,
            isHeader: true,
            press: () => _controller.setMenuIndex(context, index),
          ),
        ),
      ),
    );
  }
}

/*class WebMenuItem extends StatefulWidget {
  const WebMenuItem({
    Key? key,
    required this.isActive,
    required this.text,
    required this.press,
  }) : super(key: key);

  final bool isActive;
  final String text;
  final VoidCallback press;

  @override
  _WebMenuItemState createState() => _WebMenuItemState();
}

class _WebMenuItemState extends State<WebMenuItem> {
  bool _isHover = false;

  Color _borderColor() {
    if (widget.isActive) {
      return mcPrimaryColor;
    } else if (!widget.isActive & _isHover) {
      return mcPrimaryColor.withOpacity(0.4);
    }
    return Colors.transparent;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.press,
      onHover: (value) {
        setState(() {
          _isHover = value;
        });
      },
      child: AnimatedContainer(
        duration: kDefaultDuration,
        margin: EdgeInsets.symmetric(horizontal: mdDefaultPadding),
        padding: EdgeInsets.symmetric(vertical: mdDefaultPadding / 2),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: _borderColor(), width: 3),
          ),
        ),
        child: Text(
          widget.text,
          style: TextStyle(
            color: widget.isActive ? mcPrimaryColor : mcTextColor,
            fontWeight: widget.isActive ? FontWeight.w600 : FontWeight.normal,
          ),
        ),
      ),
    );
  }
}
*/
