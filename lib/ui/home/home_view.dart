import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/api/roles_api.dart';
import 'package:starlinx_support/api/time_zone_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/controllers/MenuController.dart';
import 'package:starlinx_support/providers/menu_selected.dart';
import 'package:starlinx_support/services/user_service.dart';

import 'components/header.dart';
import 'components/side_menu.dart';

class HomeView extends StatefulWidget {
  HomeView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  final MenuController _controller = Get.put(MenuController());
  final _apiFleets = locator<FleetsApi>();
  final _apiTimeZone = locator<TimeZoneApi>();
  final _apiRoles = locator<RolesApi>();

  final log = getLogger('HomeView');
  final _userService = locator<UserService>();

  bool _isLoading = true;

  _initData() async {
    setState(() => _isLoading = true);

    final currentUser = _userService.currentUser;
    if (currentUser != null) {
      if (currentUser.isViewProject()) {
        await _apiFleets.getItems(true);
      }
    }

    await _apiTimeZone.getItems(true);
    await _apiRoles.getItems(true);

    setState(() => _isLoading = false);
  }

  @override
  void initState() {
    super.initState();

    _initData();
  }

  @override
  Widget build(BuildContext context) {
    int _menuSelected =
        Provider.of<MenuSelectedProvider>(context).selectedIndex;

    int time = 0;
    return new WillPopScope(
      child: new Scaffold(
        key: _controller.scaffoldkey,
        appBar: Header(_controller),
        drawer: SideMenu(_controller),
        // drawer: (!Responsive.isDesktop(context) ? SideMenu() : null),
        // body: _controller.menuItems[_menuSelected].body,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: _isLoading
              ? _buildLoadingPage()
              : _controller.menuItems[_menuSelected].body,
        ),
      ),
      onWillPop: () async {
        if (_controller.isDrawerOpen()) {
          _controller.openOrCloseDrawer();

          return false;
        }
        if ((defaultTargetPlatform == TargetPlatform.iOS) ||
            (defaultTargetPlatform == TargetPlatform.android)) {
          if ((time - DateTime.now().millisecondsSinceEpoch).abs() > 2000) {
            time = DateTime.now().millisecondsSinceEpoch;
            Fluttertoast.showToast(
              msg: "Press Back again to exit.",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              // timeInSecForIosWeb: 0
            );
            return false;
          }
        }
        /* if (Platform.isAndroid || Platform.isIOS) {
          
        }*/

        return true;
      },
    );
  }

  Widget _buildLoadingPage() => Material(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
}
