import 'package:flutter/material.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SettingsScreenState();
  /*@override
  Widget build(BuildContext context) {
    // final MenuController _controller = Get.put(MenuController());
    // MenuSelectedProvider _menuSelected =
    // Provider.of<MenuSelectedProvider>(context);
    return ViewModelBuilder<SettingsScreenModel>.reactive(
      onModelReady: (model) =>
          SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
        model.runStartupLogic(context);
      }),
      builder: (context, model, child) => Scaffold(
        // key: _controller.scaffoldkey,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(mdDefaultPadding),
                constraints: BoxConstraints(maxWidth: mdMaxWidth),
                child: Text("Settings Screen View"),
              ),
            ],
          ),
        ),
      ),
      viewModelBuilder: () => SettingsScreenModel(),
    );
  }*/
}

class SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(mdDefaultPadding),
                constraints: BoxConstraints(maxWidth: mdMaxWidth),
                child: Text("Settings Screen View"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
