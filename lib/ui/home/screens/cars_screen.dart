import 'package:flutter/material.dart';
import 'package:starlinx_support/api/cars_api.dart';
import 'package:starlinx_support/api/drivers_api.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/api/time_zone_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/car_model.dart';
import 'package:starlinx_support/models/driver_model.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/home/components/table/datatable_componnent.dart';
import 'package:starlinx_support/ui/home/components/table/datatable_header.dart';
import 'package:starlinx_support/ui/home/dialogs/add_car_dialog.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/ui_helpers.dart';

class CarsScreen extends StatefulWidget {
  const CarsScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => CarsScreenState();
}

class CarsScreenState extends State<CarsScreen> {
  final _api = locator<CarsApi>();
  final _apiFleets = locator<FleetsApi>();
  final _apiTimeZone = locator<TimeZoneApi>();
  final _apiDrivers = locator<DriversApi>();

  final log = getLogger('CarsScreen');
  final _userService = locator<UserService>();

  List<Map<String, dynamic>> _sourceOriginal = <Map<String, dynamic>>[];
  List<Map<String, dynamic>> _sourceFiltered = <Map<String, dynamic>>[];
  List<Map<String, dynamic>> _source = <Map<String, dynamic>>[];
  late List<DatatableHeader> _headers;
  final List<int> _perPages = [10, 20, 50, 100];
  List<bool>? _expanded;

  String _mainField = 'carNumber';
  String? _sortColumn;
  int _total = 100;
  int _currentPerPage = 10;
  int _currentPage = 1;
  bool _isSearch = false;
  bool _isAdding = true;
  bool _sortAscending = true;
  bool _isLoading = true;

  List<Map<String, dynamic>> _convertData(List<CarModel> dataList) {
    List<Map<String, dynamic>> temps = <Map<String, dynamic>>[];
    for (var data in dataList) {
      temps.add(data.toJson());
    }
    return temps;
  }

  Future<List<Map<String, dynamic>>> _getItemsList() async {
    List<CarModel>? res = await _api.getItems();
    if (res != null) {
      return _convertData(res);
    }

    return [];
  }

  _initData() async {
    setState(() => _isLoading = true);

    final currentUser = _userService.currentUser;
    _isAdding = (currentUser != null && currentUser.isCreateCars());

    if (currentUser != null && currentUser.isViewDrivers()) {
      await _apiDrivers.getItems(false);
    }

    List<Map<String, dynamic>> data = await _getItemsList();
    int len = data.length;
    int range = _currentPerPage < len ? _currentPerPage : len;
    _expanded = List.generate(range, (index) => false);

    Future.delayed(const Duration(seconds: 0)).then((value) {
      _sourceOriginal.clear();
      _sourceOriginal.addAll(data);
      _sourceFiltered = _sourceOriginal;
      _total = _sourceFiltered.length;
      _source = _sourceFiltered.getRange(0, range).toList();
      setState(() => _isLoading = false);
    });
  }

  List<int>? _splitColor(String val) {
    int startIndex = val.indexOf('(');
    int endIndex = val.indexOf(')');
    String data = val.substring(startIndex + 1, endIndex);
    List<String> sp = data.split(',');

    if (sp.length == 3) {
      List<int> res = [];
      for (var item in sp) {
        item.replaceAll('%', '');
        if (int.tryParse(item) == null) {
          return null;
        }

        res.add(int.parse(item));
      }

      return res;
    }
  }

  Color _getColor(String val) {
    if (val.startsWith('hsv')) {
      List<int>? res = _splitColor(val);
      if (res != null) {
        try {
          return HSVColor.fromAHSV(
                  1, (res[0] / 100), (res[1] / 100), (res[2] / 100))
              .toColor();
        } catch (e) {
          log.e('Error:${e.toString()}');
        }
      }
      // Color.fromARGB(a, r, g, b)
    } else if (val.startsWith('rgb')) {
      List<int>? res = _splitColor(val);
      if (res != null) {
        try {
          return Color.fromRGBO(res[0], res[1], res[2], 1);
        } catch (e) {
          log.e('Error:${e.toString()}');
        }
      }
    }

    return Colors.white;
  }

  _intDatatableHeader() {
    _headers = [
      DatatableHeader(
          text: "car number",
          value: "carNumber",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "car id",
          value: "carId",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "device id",
          value: "deviceId",
          show: true,
          flex: 1,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "fleet",
          value: "projectId",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            return Container(
              child: Text(_apiFleets.getFleetNameById(value),
                  textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "time zone",
          value: "timeZone",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            return Container(
              child: Text(_apiTimeZone.getZoneNameById(value),
                  textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "car type",
          value: "carType",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "sub type",
          value: "subType",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "car year",
          value: "carYear",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "car color",
          value: "carColor",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            return Container(
              child: Text(value,
                  style: TextStyle(color: _getColor(value)),
                  textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "sim imsi",
          value: "simImsi",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "sim phone",
          value: "simPhone",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "driver ids",
          value: "driverIds",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            String name = '';

            try {
              if (value.toString().isNotEmpty) {
                List<dynamic> list = List<dynamic>.from(value);
                List<DriverModel> drivers =
                    _apiDrivers.getDriversName(list.cast<int>());
                for (var i = 0; i < drivers.length; i++) {
                  if (name.isNotEmpty) {
                    name += ', ';
                  }
                  name += '${drivers[i].firstName} ${drivers[i].lastName}';
                }
              }
              //List<int> val = (value.Select(x => x?.ToString()).ToList();

            } catch (e) {
              log.e('Error:${e.toString()}');
            }

            return Container(
              child: Text(name, textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
    ];
  }

  @override
  void initState() {
    super.initState();
    _initData();
    _intDatatableHeader();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _filterData(value) {
    setState(() => _isLoading = true);

    try {
      if (value == "" || value == null) {
        _sourceFiltered = _sourceOriginal;
      } else {
        _sourceFiltered = _sourceOriginal.where((data) {
          for (int i = 0; i < _headers.length; i++) {
            if (_headers[i].show &&
                data[_headers[i].value]
                    .toString()
                    .toLowerCase()
                    .contains(value.toString().toLowerCase())) {
              return true;
            }
          }

          return false;
        }).toList();
      }

      _total = _sourceFiltered.length;
      var _rangeTop = _total < _currentPerPage ? _total : _currentPerPage;
      _expanded = List.generate(_rangeTop, (index) => false);
      _source = _sourceFiltered.getRange(0, _rangeTop).toList();

      _currentPage = 1;
      _resetData();
    } catch (e) {
      print(e);
    }
    setState(() => _isLoading = false);
  }

  _resetData({int start = 0}) async {
    setState(() => _isLoading = true);
    int _expandedLen =
        _total - start < _currentPerPage ? _total - start : _currentPerPage;
    Future.delayed(const Duration(seconds: 0)).then((value) {
      _expanded = List.generate(_expandedLen, (index) => false);
      _source.clear();
      _source = _sourceFiltered.getRange(start, start + _expandedLen).toList();
      setState(() => _isLoading = false);
    });
  }

  _addItem() async {
    CarModel? data = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AddCarDialogs(
            title: 'ADD NEW VEHICLE',
            btnText: "SAVE VEHICLE",
          );
        });

    if (data != null) {
      _initData();
    }
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  Widget _buildBody() => Scaffold(
        body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
              Container(
                margin: const EdgeInsets.all(10),
                padding: const EdgeInsets.all(0),
                constraints:
                    BoxConstraints(maxHeight: screenHeight(context) - 110),
                child: Card(
                  elevation: 1,
                  shadowColor: Colors.black,
                  color: mcBackgroundColor,
                  clipBehavior: Clip.none,
                  child: ResponsiveDatatable(
                    title: "VEHICLES",
                    headers: _headers,
                    source: _source,
                    actions: [
                      if (_isSearch)
                        Expanded(
                            child: TextField(
                          autofocus: true,
                          decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            hintText: 'search...'.toUpperCase(),
                            prefixIcon: IconButton(
                                icon: const Icon(Icons.cancel),
                                onPressed: () {
                                  _filterData("");
                                  setState(() {
                                    _isSearch = false;
                                  });
                                }),
                            /*suffixIcon: IconButton(
                                  icon: const Icon(Icons.search),
                                  onPressed: () {})*/
                          ),
                          onChanged: (value) {
                            _filterData(value);
                          },
                        )),
                      if (!_isSearch)
                        IconButton(
                            icon: const Icon(Icons.search),
                            onPressed: () {
                              setState(() {
                                _isSearch = true;
                              });
                            }),
                      Row(children: [
                        IconButton(
                            icon: const Icon(Icons.refresh),
                            onPressed: () {
                              _initData();
                            }),
                        if (_isAdding)
                          IconButton(
                              icon: const Icon(Icons.add), onPressed: _addItem)
                      ])
                    ],
                    mainField: _mainField,
                    onTabRow: (data) {
                      print(data);
                    },
                    onSort: (value) {
                      setState(() => _isLoading = true);

                      setState(() {
                        _sortColumn = value;
                        _sortAscending = !_sortAscending;
                        if (_sortAscending) {
                          _sourceFiltered.sort((a, b) => b[_sortColumn]
                              .toString()
                              .compareTo(a[_sortColumn].toString()));
                        } else {
                          _sourceFiltered.sort((a, b) => a[_sortColumn]
                              .toString()
                              .compareTo(b[_sortColumn].toString()));
                        }
                        int _rangeTop = _currentPerPage < _sourceFiltered.length
                            ? _currentPerPage
                            : _sourceFiltered.length;
                        _source =
                            _sourceFiltered.getRange(0, _rangeTop).toList();
                        //_searchKey = value;

                        _isLoading = false;
                      });
                    },
                    expanded: _expanded,
                    sortAscending: _sortAscending,
                    sortColumn: _sortColumn,
                    isLoading: _isLoading,
                    footers: [
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: const Text("Rows show:"),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: DropdownButton(
                            value: _currentPerPage,
                            items: _perPages
                                .map((e) => DropdownMenuItem(
                                      child: Text("$e"),
                                      value: e,
                                    ))
                                .toList(),
                            onChanged: (int? value) {
                              setState(() {
                                _currentPerPage = value!;
                                _currentPage = _currentPage -
                                    (_currentPage % _currentPerPage) +
                                    1;
                                _resetData(start: _currentPage - 1);
                              });
                            }),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: Text(
                            "$_currentPage - ${(_currentPage + (_currentPerPage - 1)) < _total ? _currentPage + (_currentPerPage - 1) : _total} of $_total"),
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          size: 16,
                        ),
                        onPressed: _currentPage == 1
                            ? null
                            : () {
                                var _nextSet = _currentPage - _currentPerPage;
                                setState(() {
                                  _currentPage = _nextSet > 1 ? _nextSet : 1;
                                  print(_currentPage);

                                  _resetData(start: _currentPage - 1);
                                });
                              },
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                      ),
                      IconButton(
                        icon: const Icon(Icons.arrow_forward_ios, size: 16),
                        onPressed: _currentPage + _currentPerPage - 1 > _total
                            ? null
                            : () {
                                var _nextSet = _currentPage + _currentPerPage;
                                print('_nextSet=$_nextSet');

                                setState(() {
                                  _currentPage = _nextSet <= _total
                                      ? _nextSet
                                      : _total - _currentPerPage;
                                  print(_currentPage);
                                  _resetData(start: _nextSet - 1);
                                });
                              },
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                      )
                    ],
                  ),
                ),
              ),
            ])),
      );
}
