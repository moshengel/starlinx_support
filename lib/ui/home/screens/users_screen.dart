import 'package:flutter/material.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/api/roles_api.dart';
import 'package:starlinx_support/api/time_zone_api.dart';
import 'package:starlinx_support/api/users_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/user_model.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/home/components/table/datatable_componnent.dart';
import 'package:starlinx_support/ui/home/components/table/datatable_header.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/ui_helpers.dart';

class UsersScreen extends StatefulWidget {
  const UsersScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => UsersScreenState();
}

class UsersScreenState extends State<UsersScreen> {
  final _api = locator<UsersApi>();
  final _apiFleets = locator<FleetsApi>();
  final _apiTimeZone = locator<TimeZoneApi>();
  final _apiRoles = locator<RolesApi>();

  final log = getLogger('UsersScreen');
  final _userService = locator<UserService>();

  List<Map<String, dynamic>> _sourceOriginal = <Map<String, dynamic>>[];
  List<Map<String, dynamic>> _sourceFiltered = <Map<String, dynamic>>[];
  List<Map<String, dynamic>> _source = <Map<String, dynamic>>[];
  late List<DatatableHeader> _headers;
  final List<int> _perPages = [10, 20, 50, 100];
  List<bool>? _expanded;

  String _mainField = 'name';
  String? _sortColumn;
  int _total = 100;
  int _currentPerPage = 10;
  int _currentPage = 1;
  bool _isSearch = false;
  bool _isAdding = true;
  bool _sortAscending = true;
  bool _isLoading = true;

  List<Map<String, dynamic>> _convertData(List<UserModel> dataList) {
    List<Map<String, dynamic>> temps = <Map<String, dynamic>>[];
    for (var data in dataList) {
      temps.add(data.toJson());
    }
    return temps;
  }

  Future<List<Map<String, dynamic>>> _getItemsList() async {
    List<UserModel>? res = await _api.getItems();
    if (res != null) {
      return _convertData(res);
    }

    return [];
  }

  _initData() async {
    setState(() => _isLoading = true);

    final currentUser = _userService.currentUser;
    _isAdding = (currentUser != null && currentUser.isCreateUsers());

    List<Map<String, dynamic>> data = await _getItemsList();
    int len = data.length;
    int range = _currentPerPage < len ? _currentPerPage : len;
    _expanded = List.generate(range, (index) => false);

    Future.delayed(const Duration(seconds: 0)).then((value) {
      _sourceOriginal.clear();
      _sourceOriginal.addAll(data);
      _sourceFiltered = _sourceOriginal;
      _total = _sourceFiltered.length;
      _source = _sourceFiltered.getRange(0, range).toList();
      setState(() => _isLoading = false);
    });
  }

  _intDatatableHeader() {
    _headers = [
      DatatableHeader(
          text: "first name",
          value: "firstName",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "last name",
          value: "lastName",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "name",
          value: "name",
          show: true,
          flex: 1,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "user id",
          value: "userId",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "active",
          value: "active",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            bool val = value != null && (value as bool);
            return Container(
              child: Icon(val ? Icons.check : Icons.close,
                  color: val ? Colors.green : Colors.red),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "role id",
          value: "roleId",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            return Container(
              child: Text(_apiRoles.getRoleNameById(value),
                  textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "time zone",
          value: "timeZone",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            return Container(
              child: Text(_apiTimeZone.getZoneNameById(value),
                  textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "email",
          value: "email",
          show: true,
          sortable: true,
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "fleet",
          value: "projectId",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            return Container(
              child: Text(_apiFleets.getFleetNameById(value),
                  textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center),
      DatatableHeader(
          text: "fleets",
          value: "projectIds",
          show: true,
          sortable: true,
          sourceBuilder: (dynamic value, Map<String, dynamic> row) {
            List<dynamic> list = List<dynamic>.from(value);
            String data = '';
            for (var i = 0; i < list.length; i++) {
              if (data.isNotEmpty) {
                data += ', ';
              }
              data += '${_apiFleets.getFleetNameById(list[i])}';
            }

            return Container(
              child: Text(data, textAlign: TextAlign.center),
            );
          },
          textAlign: TextAlign.center)
    ];
  }

  @override
  void initState() {
    super.initState();
    _initData();
    _intDatatableHeader();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _filterData(value) {
    setState(() => _isLoading = true);

    try {
      if (value == "" || value == null) {
        _sourceFiltered = _sourceOriginal;
      } else {
        _sourceFiltered = _sourceOriginal.where((data) {
          for (int i = 0; i < _headers.length; i++) {
            if (_headers[i].show &&
                data[_headers[i].value]
                    .toString()
                    .toLowerCase()
                    .contains(value.toString().toLowerCase())) {
              return true;
            }
          }

          return false;
        }).toList();
      }

      _total = _sourceFiltered.length;
      var _rangeTop = _total < _currentPerPage ? _total : _currentPerPage;
      _expanded = List.generate(_rangeTop, (index) => false);
      _source = _sourceFiltered.getRange(0, _rangeTop).toList();

      _currentPage = 1;
      _resetData();
    } catch (e) {
      print(e);
    }
    setState(() => _isLoading = false);
  }

  _resetData({int start = 0}) async {
    setState(() => _isLoading = true);
    int _expandedLen =
        _total - start < _currentPerPage ? _total - start : _currentPerPage;
    Future.delayed(const Duration(seconds: 0)).then((value) {
      _expanded = List.generate(_expandedLen, (index) => false);
      _source.clear();
      _source = _sourceFiltered.getRange(start, start + _expandedLen).toList();
      setState(() => _isLoading = false);
    });
  }

  _addItem() async {
    /* CarModel? data = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AddCarDialogs(
            title: 'ADD NEW VEHICLE',
            btnText: "SAVE VEHICLE",
          );
        });

    if (data != null) {
      _initData();
    }*/
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  Widget _buildBody() => Scaffold(
        body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
              Container(
                margin: const EdgeInsets.all(10),
                padding: const EdgeInsets.all(0),
                constraints:
                    BoxConstraints(maxHeight: screenHeight(context) - 110),
                child: Card(
                  elevation: 1,
                  shadowColor: Colors.black,
                  color: mcBackgroundColor,
                  clipBehavior: Clip.none,
                  child: ResponsiveDatatable(
                    title: "USERS",
                    headers: _headers,
                    source: _source,
                    actions: [
                      if (_isSearch)
                        Expanded(
                            child: TextField(
                          decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            hintText: 'search...'.toUpperCase(),
                            prefixIcon: IconButton(
                                icon: const Icon(Icons.cancel),
                                onPressed: () {
                                  _filterData("");
                                  setState(() {
                                    _isSearch = false;
                                  });
                                }),
                            /* suffixIcon: IconButton(
                                  icon: const Icon(Icons.search),
                                  onPressed: () {})*/
                          ),
                          onChanged: (value) {
                            _filterData(value);
                          },
                        )),
                      if (!_isSearch)
                        IconButton(
                            icon: const Icon(Icons.search),
                            onPressed: () {
                              setState(() {
                                _isSearch = true;
                              });
                            }),
                      Row(children: [
                        IconButton(
                            icon: const Icon(Icons.refresh),
                            onPressed: () {
                              _initData();
                            }),
                        if (_isAdding)
                          IconButton(
                              icon: const Icon(Icons.add), onPressed: _addItem)
                      ])
                    ],
                    mainField: _mainField,
                    onTabRow: (data) {
                      print(data);
                    },
                    onSort: (value) {
                      setState(() => _isLoading = true);

                      setState(() {
                        _sortColumn = value;
                        _sortAscending = !_sortAscending;
                        if (_sortAscending) {
                          _sourceFiltered.sort((a, b) => b[_sortColumn]
                              .toString()
                              .compareTo(a[_sortColumn].toString()));
                        } else {
                          _sourceFiltered.sort((a, b) => a[_sortColumn]
                              .toString()
                              .compareTo(b[_sortColumn].toString()));
                        }
                        int _rangeTop = _currentPerPage < _sourceFiltered.length
                            ? _currentPerPage
                            : _sourceFiltered.length;
                        _source =
                            _sourceFiltered.getRange(0, _rangeTop).toList();
                        //_searchKey = value;

                        _isLoading = false;
                      });
                    },
                    expanded: _expanded,
                    sortAscending: _sortAscending,
                    sortColumn: _sortColumn,
                    isLoading: _isLoading,
                    footers: [
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: const Text("Rows show:"),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: DropdownButton(
                            value: _currentPerPage,
                            items: _perPages
                                .map((e) => DropdownMenuItem(
                                      child: Text("$e"),
                                      value: e,
                                    ))
                                .toList(),
                            onChanged: (int? value) {
                              setState(() {
                                _currentPerPage = value!;
                                _currentPage = _currentPage -
                                    (_currentPage % _currentPerPage) +
                                    1;
                                _resetData(start: _currentPage - 1);
                              });
                            }),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                        child: Text(
                            "$_currentPage - ${(_currentPage + (_currentPerPage - 1)) < _total ? _currentPage + (_currentPerPage - 1) : _total} of $_total"),
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          size: 16,
                        ),
                        onPressed: _currentPage == 1
                            ? null
                            : () {
                                var _nextSet = _currentPage - _currentPerPage;
                                setState(() {
                                  _currentPage = _nextSet > 1 ? _nextSet : 1;
                                  print(_currentPage);

                                  _resetData(start: _currentPage - 1);
                                });
                              },
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                      ),
                      IconButton(
                        icon: const Icon(Icons.arrow_forward_ios, size: 16),
                        onPressed: _currentPage + _currentPerPage - 1 > _total
                            ? null
                            : () {
                                var _nextSet = _currentPage + _currentPerPage;
                                print('_nextSet=$_nextSet');

                                setState(() {
                                  _currentPage = _nextSet <= _total
                                      ? _nextSet
                                      : _total - _currentPerPage;
                                  print(_currentPage);
                                  _resetData(start: _nextSet - 1);
                                });
                              },
                        padding: const EdgeInsets.symmetric(horizontal: 6),
                      )
                    ],
                  ),
                ),
              ),
            ])),
      );
}
