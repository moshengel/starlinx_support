import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/fleet_model.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/ui/shared/ui_helpers.dart';
import 'package:starlinx_support/ui/widgets/widgets.dart';

class AddFleetDialogs extends StatefulWidget {
  final String title, btnText;

  const AddFleetDialogs({Key? key, required this.title, required this.btnText})
      : super(key: key);

  @override
  _AddFleetDialogsState createState() => _AddFleetDialogsState();
}

class _AddFleetDialogsState extends State<AddFleetDialogs> {
  final log = getLogger('AddFleetDialogs');

  final _apiFleets = locator<FleetsApi>();

  @override
  void initState() {
    super.initState();
  }

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  late String _name;
  bool _isSaving = false;

  Widget _contentBox(BuildContext context) {
    double _screenWidth = screenWidth(context);

    final nameField = TextFormField(
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter fleet name" : null,
      onSaved: (value) => _name = value!,
      style: TextStyle(color: mcTextColorDark),
      decoration:
          buildInputDecorationWhite("Name", "Enter name...", null, null, null),
    );
    VoidCallback doSave = () async {
      FocusManager.instance.primaryFocus?.unfocus();

      final form = _formKey.currentState;
      if (form!.validate()) {
        form.save();

        setState(
          () {
            _isSaving = true;
          },
        );

        FleetModel? res = await _apiFleets.addItem(_name);
        if (res != null) {
          Navigator.of(context).pop(res);
        } else {
          setState(
            () {
              _isSaving = false;
            },
          );
        }
      } else {
        print("form is invalid");
      }
    };
    var loading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        horizontalSpaceRegular,
        labeWhite("Saving... Please wait", null, null)
      ],
    );

    return Stack(
      children: <Widget>[
        Container(
            width: _screenWidth < 500.0 ? _screenWidth : 500.0,
            padding: EdgeInsets.all(mdDefaultPadding),
            margin: EdgeInsets.only(top: mdDefaultPadding),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: mcBackgroundColorWhite,
                borderRadius: BorderRadius.circular(mdRadiusCircular),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black,
                      offset: Offset(1.0, 2.0),
                      blurRadius: mdRadiusCircular),
                ]),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    labeWhite(widget.title, FontWeight.w600, 22.0),
                    verticalSpaceRegular,
                    nameField,
                    verticalSpaceRegular,
                    _isSaving ? loading : longButtons(widget.btnText, doSave)
                  ],
                ),
              ),
            )),
        Positioned(
          top: mdDefaultPadding,
          right: 0.0,
          child: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();

              Navigator.of(context).pop(null);
            },
            child: Align(
              alignment: Alignment.topRight,
              child: CircleAvatar(
                radius: 28.0,
                backgroundColor: Colors.transparent,
                child: Icon(Icons.close, color: mcTextColorDark),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(mdRadiusCircular),
            ),
            elevation: 10,
            backgroundColor: Colors.transparent,
            child: GestureDetector(
              onTap: () {
                FocusManager.instance.primaryFocus?.unfocus();
              },
              child: _contentBox(context),
            )));
    /*return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(mdRadiusCircular),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );*/
  }
}
