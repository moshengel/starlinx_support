import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:starlinx_support/api/cars_api.dart';
import 'package:starlinx_support/api/drivers_api.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/api/time_zone_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/car_model.dart';
import 'package:starlinx_support/models/driver_model.dart';
import 'package:starlinx_support/models/fleet_model.dart';
import 'package:starlinx_support/models/time_zone_model.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/home/components/multi_selecte/multi_selected.dart';
import 'package:starlinx_support/ui/home/components/qr_view.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/ui/shared/ui_helpers.dart';
import 'package:starlinx_support/ui/widgets/widgets.dart';
import 'package:flutter/foundation.dart';

import 'add_driver_dialog.dart';
import 'add_fleet_dialog.dart';

class AddCarDialogs extends StatefulWidget {
  final String title, btnText;
  final int? fleetId;
  const AddCarDialogs(
      {Key? key, required this.title, required this.btnText, this.fleetId})
      : super(key: key);

  @override
  _AddCarDialogsState createState() => _AddCarDialogsState();
}

class _AddCarDialogsState extends State<AddCarDialogs> {
  final log = getLogger('AddCarDialogs');

  final _apiTimeZone = locator<TimeZoneApi>();
  final _apiFleets = locator<FleetsApi>();
  final _apiDriver = locator<DriversApi>();

  final _apiCars = locator<CarsApi>();
  final _userService = locator<UserService>();

  late String _licensePlate, _make, _model, _color, _starLinkID;
  late int _year, _zoneId;

  bool _isSaving = false;

  Future<List<TimeZoneModel>> _getTimeZoonListItemsList() async {
    List<TimeZoneModel>? res = await _apiTimeZone.getItems(false);
    if (res != null) {
      return res;
    }

    return Future.value([]);
  }

  Future<List<FleetModel>> _getFleetsItemsList() async {
    var currentUser = _userService.currentUser;
    if (currentUser != null && currentUser.isViewProject()) {
      List<FleetModel>? res = await _apiFleets.getItems(false);
      if (res != null) {
        return res;
      }
    }

    return Future.value([]);
  }

  Future<List<DriverModel>> _getDriversItemsList() async {
    var currentUser = _userService.currentUser;
    if (currentUser != null && currentUser.isViewProject()) {
      List<DriverModel>? res = await _apiDriver.getItems(false);
      if (res != null) {
        return res;
      }
    }

    return Future.value([]);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /*@override
  Widget build(BuildContext context) {
    return FutureBuilder<List<TimeZoneModel>>(
      future: _timeZoonList,
      builder: (context, snapshot) {
        // print(snapshot);
        if (snapshot.hasData) {
          List<TimeZoneModel> timeZones = snapshot.data!;
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(mdRadiusCircular),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: GestureDetector(
              onTap: () {
                FocusManager.instance.primaryFocus?.unfocus();
              },
              child: contentBox(context, timeZones),
            ),
            // child: contentBox(context, timeZones),
          );
        } else if (snapshot.hasError)
          return _buildErrorPage(snapshot.error);
        else
          return _buildLoadingPage();
      },
    );
  }
*/
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormFieldState> _keyFieldTimeZone =
      GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _keyFieldFleet = GlobalKey<FormFieldState>();
  // final GlobalKey<FormFieldState> _keyDriverField = GlobalKey<FormFieldState>();

  Color _currentColor = Colors.white;
  TimeZoneModel? _timeZoneSelected;
  String _starLinxId = '';
  FleetModel? _fleetSelected;
  // DriverModel? _driverSelected;
  late int _fleetId;
  List<String> _driverId = [];
  _contentBox(BuildContext context, List<TimeZoneModel> timeZones,
      List<FleetModel> fleets, List<DriverModel> drivers) {
    String _convertColorToString(Color? color) {
      if (color == null || color == Colors.white) {
        return "";
      }
      return "rgb(${color.red.toString()}, ${color.green.toString()}, ${color.blue.toString()})";
    }

    List<DropdownMenuItem<FleetModel>> _getFleetsMenu() {
      List<DropdownMenuItem<FleetModel>> res = [];

      final currentUser = _userService.currentUser;
      if (currentUser != null && currentUser.isCreateProject()) {
        res.add(DropdownMenuItem<FleetModel>(
          value: FleetModel(name: 'ADD NEW FLEET', projectId: -1),
          child: Row(children: [
            Icon(Icons.add, color: mcPrimaryColor),
            SizedBox(width: 10.0),
            Text('ADD NEW FLEET',
                style: TextStyle(color: mcPrimaryColor),
                overflow: TextOverflow.ellipsis)
          ]),
        ));
      }

      res.addAll(fleets.map<DropdownMenuItem<FleetModel>>((FleetModel value) {
        return DropdownMenuItem<FleetModel>(
          value: value,
          child: Text(value.name,
              style: TextStyle(color: mcTextColorDark),
              overflow: TextOverflow.ellipsis),
        );
      }).toList());

      return res;
    }

    final licensePlateField = TextFormField(
      autofocus: false,
      validator: (value) =>
          value!.isEmpty ? "Please enter license plate" : null,
      onSaved: (value) => _licensePlate = value!,
      style: TextStyle(color: mcTextColorDark),
      decoration: buildInputDecorationWhite(
          "License Plate", "Enter license plate...", null, null, null),
    );
    final makeField = TextFormField(
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter make" : null,
      onSaved: (value) => _make = value!,
      style: TextStyle(color: mcTextColorDark),
      decoration:
          buildInputDecorationWhite("Make", "Enter make...", null, null, null),
    );
    final modelField = TextFormField(
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter model" : null,
      onSaved: (value) => _model = value!,
      style: TextStyle(color: mcTextColorDark),
      decoration: buildInputDecorationWhite(
          "Model", "Enter model...", null, null, null),
    );
    final yearField = TextFormField(
        autofocus: false,
        validator: (value) =>
            value == null || value.length != 4 ? "Please enter year" : null,
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        ],
        onSaved: (value) => _year = int.parse(value!),
        style: TextStyle(color: mcTextColorDark),
        decoration: buildInputDecorationWhite(
            "Year", "Enter year...", null, null, null));
    final colorField = TextFormField(
      controller:
          TextEditingController(text: _convertColorToString(_currentColor)),
      focusNode: new AlwaysDisabledFocusNode(),
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter color" : null,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        showDialog(
          context: context,
          builder: (BuildContext context) {
            void changeColor(Color color) =>
                setState(() => _currentColor = color);
            return AlertDialog(
              //titlePadding: const EdgeInsets.all(0.0),
              contentPadding: const EdgeInsets.all(0.0),
              content: SingleChildScrollView(
                child: BlockPicker(
                  pickerColor: _currentColor,
                  onColorChanged: changeColor,
                  // enableLabel: true,
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('close'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      },
      onSaved: (value) => {_color = value!},
      style: TextStyle(color: _currentColor),
      decoration: buildInputDecorationWhite(
          "Color", "Enter color...", null, null, null),
    );
    FocusNode _getFocus() {
      if ((defaultTargetPlatform == TargetPlatform.iOS) ||
          (defaultTargetPlatform == TargetPlatform.android)) {
        return new AlwaysDisabledFocusNode();
      }

      return FocusNode();
    }

    String _fixStarLinkId(String? value) {
      if (value != null && value.length == 17) {
        String code = value.replaceAll(":", "");
        if (code.length == 12) {
          return 'TAG$code';
        }
      }

      return '';
    }

    final starLinkIdField = TextFormField(
      controller: TextEditingController(text: _fixStarLinkId(_starLinxId)),
      autofocus: false,
      focusNode: _getFocus(),
      validator: (value) => (value!.isEmpty || value.length != 15)
          ? "Please enter StarLink ID"
          : null,
      onSaved: (value) {
        _starLinkID = value!;
      },
      onTap: () {
        if ((defaultTargetPlatform == TargetPlatform.iOS) ||
            (defaultTargetPlatform == TargetPlatform.android)) {
          FocusManager.instance.primaryFocus?.unfocus();

          showDialog(
            context: context,
            builder: (BuildContext context) {
              void onChange(String data) => setState(() => _starLinxId = data);
              return Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(mdRadiusCircular),
                ),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: QRViewComponent(
                  onChanged: onChange,
                ),
              );
              /*return AlertDialog(
                  // titlePadding: const EdgeInsets.all(0.0),
                  contentPadding: const EdgeInsets.all(0.0),
                  content: QRViewComponent());*/
            },
          );
        }
      },
      style: TextStyle(color: mcTextColorDark),
      decoration: buildInputDecorationWhite(
          "StarLink ID", "Enter StarLink ID...", null, null, null),
      onChanged: (value) {
        if ((defaultTargetPlatform != TargetPlatform.iOS) &&
            (defaultTargetPlatform != TargetPlatform.android)) {
          if (value.length == 17) {
            String code = value.replaceAll(":", "");
            if (code.length == 12) {
              setState(() {
                _starLinxId = value;
              });
            }
          }
        }
      },
    );
    final selectedTimeZoneField = DropdownButtonFormField<TimeZoneModel>(
      key: _keyFieldTimeZone,
      autofocus: false,
      iconEnabledColor: mcTextColorDark,
      dropdownColor: mcBackgroundColorWhite,
      // menuMaxHeight: 550,
      decoration: buildDropdownDecorationWhite(
          "Time Zone", "Select time zone...", null),
      value: _timeZoneSelected,
      validator: (value) => value == null ? "Please select time zone" : null,
      isExpanded: true,
      items: timeZones.map((TimeZoneModel value) {
        return DropdownMenuItem<TimeZoneModel>(
          value: value,
          child: new Text(value.zoneName,
              style: TextStyle(color: mcTextColorDark)),
        );
      }).toList(),
      onChanged: (val) {
        setState(
          () {
            _timeZoneSelected = val;
          },
        );
      },
      onSaved: (value) => _zoneId = value!.zoneId,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
    );
    final selectedFleetField = DropdownButtonFormField<FleetModel>(
      key: _keyFieldFleet,
      autofocus: false,
      iconEnabledColor: mcTextColorDark,
      dropdownColor: mcBackgroundColorWhite,
      // menuMaxHeight: 450,
      decoration:
          buildDropdownDecorationWhite("Fleet", "Select fleet...", null),
      value: _fleetSelected,
      validator: (value) => value == null ? "Please select fleet" : null,
      isExpanded: true,
      items: _getFleetsMenu(),
      onChanged: (val) async {
        FleetModel? data = val;
        if (val != null && val.projectId == -1) {
          _keyFieldFleet.currentState!.reset();
          data = await showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AddFleetDialogs(
                  title: val.name,
                  btnText: "SAVE FLEET",
                );
              });

          if (data != null) {
            fleets.add(data);
          }
        }
        setState(
          () {
            _fleetSelected = data;
          },
        );
      },
      onSaved: (value) => _fleetId = value!.projectId,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
    );

    /*List<DropdownMenuItem<DriverModel>> _getDriversMenu() {
      List<DropdownMenuItem<DriverModel>> res = [];
      final currentUser = _userService.currentUser;
      if (currentUser != null && currentUser.isCreateDrivers()) {
        DriverModel addNew = new DriverModel(
            driverId: -1,
            firstName: 'ADD NEW DRIVER',
            lastName: '',
            phoneNumber: '',
            ident: '',
            projectId: -1);
        res.add(DropdownMenuItem<DriverModel>(
          value: addNew,
          child: new Row(children: [
            Icon(Icons.add, color: mcPrimaryColor),
            SizedBox(width: 10.0),
            Text(addNew.firstName,
                style: TextStyle(color: mcPrimaryColor),
                overflow: TextOverflow.ellipsis)
          ]),
        ));
      }

      for (var item in drivers) {
        if (_fleetSelected != null &&
            item.projectId == _fleetSelected!.projectId) {
          res.add(DropdownMenuItem<DriverModel>(
            value: item,
            child: new Text('${item.firstName} ${item.lastName}',
                style: TextStyle(color: mcTextColorDark),
                overflow: TextOverflow.ellipsis),
          ));
        }
      }

      return res;
    }
*/
    final currentUser = _userService.currentUser;

    final selectedDriverField = MultiSelected(
      isAdding: (currentUser != null && currentUser.isCreateDrivers()),
      isDisable: (_fleetSelected == null),
      // autovalidateMode: AutovalidateMode.always,
      initialValue: _driverId,
      titleText: 'Select a driver(s)',
      maxLength: 10, // optional
      maxLengthText: '',
      validator: (dynamic value) {
        return value == null ? 'Please select a driver(s)' : null;
      },
      errorText: 'Please select one or more option(s)',
      dataSource: drivers
          .where((element) =>
              _fleetSelected != null &&
              element.projectId == _fleetSelected!.projectId)
          .map((item) {
        return item.toJson();
      }).toList(),
      textField: ['firstName', 'lastName'],
      valueField: 'driverId',
      filterable: true,
      required: false,
      onSaved: (value) async {
        try {
          if (value == null) {
            _driverId = [];
          } else {
            List<int> list = List<int>.from(value);
            if (list.length == 1 && list[0] == -1) {
              _driverId = [];

              DriverModel? data = await showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AddDriverDialog(
                      title: 'ADD NEW DRIVER',
                      fleetSelected: _fleetSelected,
                      btnText: "SAVE DRIVER",
                    );
                  });

              if (data != null) {
                setState(
                  () {
                    _driverId = [data.driverId.toString()];
                  },
                );
                // drivers.add(data);
              }
            } else {
              _driverId = list.map((e) => e.toString()).toList();
            }
            print('The saved values are $value');
          }
        } catch (e) {
          log.e('Error ${e.toString()}');
        }
      },
      change: (value) {
        print('The selected values are $value');
      },
      selectIcon: Icons.arrow_drop_down,
      selectIconColor: mcTextColorDark,
      titleTextColor: mcTextColorDark,

      //selectedOptionsInfoTextColor: mcTextColorDark,
      // selectedOptionsBoxColor: mcBackgroundColorWhite,
      // saveButtonColor: Theme.of(context).primaryColor,
      //checkBoxColor: Theme.of(context).primaryColorDark,
      //cancelButtonColor: Theme.of(context).primaryColorLight,
      responsiveDialogSize: Size(480, 600),

      buttonBarColor: mcBackgroundColorWhite,
    );

/*final selectedDriverField = DropdownButtonFormField<DriverModel>(
      key: _keyDriverField,
      autofocus: false,
      iconEnabledColor: mcTextColorDark,
      dropdownColor: mcBackgroundColorWhite,
      // menuMaxHeight: 250,
      decoration:
          buildDropdownDecorationWhite("Driver", "Select a driver...", null),
      value: _driverSelected,
      //validator: (value) => value == null ? "Please select a driver" : null,
      isExpanded: true,
      items: _getDriversMenu(),
      onChanged: (_fleetSelected == null)
          ? null
          : (val) async {
              DriverModel? data = val;
              if (val != null && val.driverId == -1) {
                _keyDriverField.currentState!.reset();
                data = await showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AddDriverDialog(
                        title: val.firstName,
                        fleetSelected: _fleetSelected,
                        btnText: "SAVE DRIVER",
                      );
                    });

                if (data != null) {
                  drivers.add(data);
                }
              }

              if (data != null && data.projectId != _fleetSelected!.projectId) {
                data = null;
              }

              setState(
                () {
                  _driverSelected = data;
                },
              );
            },
      onSaved: (value) {
        _driverId.add(value!.driverId);
      },
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
    );
*/

    VoidCallback doSave = () async {
      FocusManager.instance.primaryFocus?.unfocus();

      final form = _formKey.currentState;
      if (form!.validate()) {
        form.save();

        setState(
          () {
            _isSaving = true;
          },
        );

        CarModel? res = await _apiCars.addItem(_fleetId, _licensePlate, _make,
            _model, _year, _color, _starLinkID, _zoneId, _driverId);
        if (res != null) {
          Navigator.of(context).pop(res);
        } else {
          setState(
            () {
              _isSaving = false;
            },
          );
        }
      } else {
        print("form is invalid");
      }
    };
    var loading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        horizontalSpaceRegular,
        labeWhite("Saving... Please wait", null, null)
      ],
    );
    double _screenWidth = screenWidth(context);

    return Stack(
      children: <Widget>[
        Container(
            width: _screenWidth < 500.0 ? _screenWidth : 500.0,
            padding: EdgeInsets.all(mdDefaultPadding),
            margin: EdgeInsets.only(top: mdDefaultPadding),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: mcBackgroundColorWhite,
                borderRadius: BorderRadius.circular(mdRadiusCircular),
                boxShadow: [
                  BoxShadow(
                      // color: color.withOpacity(0.8),
                      color: Colors.black,
                      offset: Offset(1.0, 2.0),
                      blurRadius: mdRadiusCircular),
                ]),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    labeWhite(widget.title, FontWeight.w600, 22.0),
                    verticalSpaceRegular,
                    licensePlateField,
                    verticalSpaceRegular,
                    makeField,
                    verticalSpaceRegular,
                    modelField,
                    verticalSpaceRegular,
                    yearField,
                    verticalSpaceRegular,
                    colorField,
                    verticalSpaceRegular,
                    starLinkIdField,
                    verticalSpaceRegular,
                    selectedTimeZoneField,
                    verticalSpaceRegular,
                    selectedFleetField,
                    verticalSpaceRegular,
                    selectedDriverField,
                    verticalSpaceRegular,
                    _isSaving ? loading : longButtons(widget.btnText, doSave),
                    verticalSpaceRegular,
                  ],
                ),
              ),
            )),
        Positioned(
          top: mdDefaultPadding,
          right: 0.0,
          child: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();

              Navigator.of(context).pop(null);
            },
            child: Align(
              alignment: Alignment.topRight,
              child: CircleAvatar(
                radius: 28.0,
                backgroundColor: Colors.transparent,
                child: Icon(Icons.close, color: mcTextColorDark),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildErrorPage(error) => Material(
        child: Center(
          child: Text("ERROR: $error"),
        ),
      );

  Widget _buildLoadingPage() => Material(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );

  Widget _getChild(context, AsyncSnapshot<List<dynamic>> snapshot) {
    if (snapshot.hasData) {
      return _contentBox(
          context, snapshot.data![0], snapshot.data![1], snapshot.data![2]);
    } else if (snapshot.hasError)
      return _buildErrorPage(snapshot.error);
    else
      return _buildLoadingPage();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([
        _getTimeZoonListItemsList(),
        _getFleetsItemsList(),
        _getDriversItemsList()
      ]),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return Scaffold(
            body: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(mdRadiusCircular),
          ),
          elevation: 10,
          backgroundColor: Colors.transparent,
          child: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            child: _getChild(context, snapshot),
          ),
        ));
      },
    );
  }
}
