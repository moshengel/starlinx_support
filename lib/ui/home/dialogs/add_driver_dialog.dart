import 'dart:convert';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:starlinx_support/api/drivers_api.dart';
import 'package:starlinx_support/api/fleets_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/models/driver_model.dart';
import 'package:starlinx_support/models/fleet_model.dart';
import 'package:starlinx_support/services/user_service.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/ui/shared/ui_helpers.dart';
import 'package:starlinx_support/ui/widgets/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'add_fleet_dialog.dart';

class AddDriverDialog extends StatefulWidget {
  final String title, btnText;
  final FleetModel? fleetSelected;
  const AddDriverDialog(
      {Key? key,
      required this.title,
      required this.btnText,
      this.fleetSelected})
      : super(key: key);

  @override
  _AddDriverDialogState createState() => _AddDriverDialogState();
}

class _AddDriverDialogState extends State<AddDriverDialog> {
  final log = getLogger('AddDriverDialog');

  final _apiFleets = locator<FleetsApi>();
  final _apiDriver = locator<DriversApi>();
  final _userService = locator<UserService>();

  Future<List<FleetModel>> _getFleetsItemsList() async {
    var currentUser = _userService.currentUser;
    if (currentUser != null && currentUser.isViewProject()) {
      List<FleetModel>? res = await _apiFleets.getItems(false);
      if (res != null) {
        return res;
      }
    }

    return Future.value([]);
  }

  Future<Map> _getCountry() async {
    http.Response data = await http.get(Uri.parse('https://api.myip.com/'));
    return jsonDecode(data.body);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<FormFieldState> _keyField = GlobalKey<FormFieldState>();
  // GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  late String _firstName, _lastName, _phoneNumber;
  late int _fleetId;
  bool _isValidPhoneNumber = false;
  FleetModel? _fleetSelected;
  bool _isSaving = false;

  Widget _contentBox(
      BuildContext context, List<FleetModel> fleets, Map localCods) {
    double _screenWidth = screenWidth(context);
    List<DropdownMenuItem<FleetModel>> _getFleetsMenu() {
      List<DropdownMenuItem<FleetModel>> res = [];

      final currentUser = _userService.currentUser;
      if (currentUser != null && currentUser.isCreateProject()) {
        res.add(DropdownMenuItem<FleetModel>(
          value: FleetModel(name: 'ADD NEW FLEET', projectId: -1),
          child: Row(children: [
            Icon(Icons.add, color: mcPrimaryColor),
            SizedBox(width: 10.0),
            Text('ADD NEW FLEET',
                style: TextStyle(color: mcPrimaryColor),
                overflow: TextOverflow.ellipsis)
          ]),
        ));
      }

      res.addAll(fleets.map<DropdownMenuItem<FleetModel>>((FleetModel value) {
        return DropdownMenuItem<FleetModel>(
          value: value,
          child: Text(value.name,
              style: TextStyle(color: mcTextColorDark),
              overflow: TextOverflow.ellipsis),
        );
      }).toList());

      return res;
    }

    if (widget.fleetSelected != null) {
      _fleetSelected = widget.fleetSelected;
    }
    final firstNameField = TextFormField(
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter Name" : null,
      onSaved: (value) => _firstName = value!,
      style: TextStyle(color: mcTextColorDark),
      decoration:
          buildInputDecorationWhite("Name", "Enter Name...", null, null, null),
    );
    final lastNameField = TextFormField(
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter Family" : null,
      onSaved: (value) => _lastName = value!,
      style: TextStyle(color: mcTextColorDark),
      decoration: buildInputDecorationWhite(
          "Family", "Enter Family...", null, null, null),
    );
    final phoneNumberField = InternationalPhoneNumberInput(
      inputDecoration:
          buildInputDecorationWhite('Phone Number', '', null, null, null),
      selectorTextStyle: TextStyle(color: mcTextColorDark),
      textStyle: TextStyle(color: mcTextColorDark),
      /* inputDecoration: InputDecoration(
          labelText: 'Phone Number',
          contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0)),*/
      onInputChanged: (PhoneNumber number) {
        print(number.phoneNumber);
      },
      onInputValidated: (bool value) {
        _isValidPhoneNumber = value;
        print(value);
      },
      selectorConfig: SelectorConfig(
          selectorType: PhoneInputSelectorType.DIALOG,
          setSelectorButtonAsPrefixIcon: true,
          leadingPadding: 20.0,
          trailingSpace: true),
      spaceBetweenSelectorAndTextField: 0.0,
      validator: (value) {
        if ((defaultTargetPlatform == TargetPlatform.iOS) ||
            (defaultTargetPlatform == TargetPlatform.android)) {
        } else {
          _isValidPhoneNumber =
              value != null && value.isNotEmpty && value.length > 7;
        }

        return !_isValidPhoneNumber ? "Please enter Number Phone" : null;
      },
      // selectorButtonOnErrorPadding: 12,
      ignoreBlank: true,
      autoValidateMode: AutovalidateMode.onUserInteraction,
      // locale: DateTime.now().timeZoneName,
      // selectorTextStyle: TextStyle(color: Colors.black),
      initialValue: PhoneNumber(
          isoCode: (localCods.containsKey("cc") ? localCods['cc'] : 'US')),
      // textFieldController: controller,
      formatInput: false,
      // textAlignVertical: TextAlignVertical.top,
      // keyboardType:
      //  TextInputType.numberWithOptions(signed: true, decimal: true),
      // inputBorder: OutlineInputBorder(),
      onSaved: (PhoneNumber number) {
        _phoneNumber = number.phoneNumber!;
        print('On Saved: $number');
      },
    );
    final selectedFleetField = DropdownButtonFormField<FleetModel>(
      key: _keyField,
      autofocus: false,
      iconEnabledColor: mcTextColorDark,
      dropdownColor: mcBackgroundColorWhite,
      menuMaxHeight: 550,
      decoration:
          buildDropdownDecorationWhite("Fleet", "Select fleet...", null),
      value: _fleetSelected,
      validator: (value) => value == null ? "Please select fleet" : null,
      isExpanded: true,
      items: _getFleetsMenu(),
      onChanged: widget.fleetSelected != null
          ? null
          : (val) async {
              FleetModel? data = val;
              if (val != null && val.projectId == -1) {
                _keyField.currentState!.reset();
                data = await showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AddFleetDialogs(
                        title: val.name,
                        btnText: "SAVE FLEET",
                      );
                    });

                if (data != null) {
                  fleets.add(data);
                }
              }
              setState(
                () {
                  _fleetSelected = data;
                },
              );
            },
      onSaved: (value) => _fleetId = value!.projectId,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
    );
    VoidCallback doSave = () async {
      FocusManager.instance.primaryFocus?.unfocus();

      final form = _formKey.currentState;
      if (form!.validate()) {
        form.save();

        setState(
          () {
            _isSaving = true;
          },
        );

        DriverModel? res = await _apiDriver.addItem(
            _firstName, _lastName, _phoneNumber, _fleetId);
        if (res != null) {
          Navigator.of(context).pop(res);
        } else {
          setState(
            () {
              _isSaving = false;
            },
          );
        }
      } else {
        print("form is invalid");
      }
    };
    var loading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        horizontalSpaceRegular,
        labeWhite("Saving... Please wait", null, null)
      ],
    );

    return Stack(
      children: <Widget>[
        Container(
            width: _screenWidth < 500.0 ? _screenWidth : 500.0,
            padding: EdgeInsets.all(mdDefaultPadding),
            margin: EdgeInsets.only(top: mdDefaultPadding),
            decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: mcBackgroundColorWhite,
                borderRadius: BorderRadius.circular(mdRadiusCircular),
                boxShadow: [
                  BoxShadow(
                      // color: color.withOpacity(0.8),
                      color: Colors.black,
                      offset: Offset(1.0, 2.0),
                      blurRadius: mdRadiusCircular),
                ]),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    labeWhite(widget.title, FontWeight.w600, 22.0),
                    verticalSpaceRegular,
                    firstNameField,
                    verticalSpaceRegular,
                    lastNameField,
                    verticalSpaceRegular,
                    phoneNumberField,
                    verticalSpaceRegular,
                    selectedFleetField,
                    verticalSpaceRegular,
                    _isSaving ? loading : longButtons(widget.btnText, doSave),
                    verticalSpaceRegular,
                  ],
                ),
              ),
            )),
        Positioned(
          top: mdDefaultPadding,
          right: 0.0,
          child: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();

              Navigator.of(context).pop(null);
            },
            child: Align(
              alignment: Alignment.topRight,
              child: CircleAvatar(
                radius: 28.0,
                backgroundColor: Colors.transparent,
                child: Icon(Icons.close, color: mcTextColorDark),
              ),
            ),
          ),
        )
      ],
    );
  }

  /* Widget _buildBody(List<FleetModel> fleets, Map localCods) => Scaffold(
          body: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(mdRadiusCircular),
        ),
        elevation: 6,
        backgroundColor: Colors.transparent,
        child: GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: contentBox(context, fleets, localCods),
        ),
      ));
*/
  Widget _buildErrorPage(error) => Material(
        child: Center(
          child: Text("ERROR: $error"),
        ),
      );

  Widget _buildLoadingPage() => Material(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );

  Widget _getChild(context, AsyncSnapshot<List<dynamic>> snapshot) {
    if (snapshot.hasData) {
      return _contentBox(context, snapshot.data![0], snapshot.data![1]);
    } else if (snapshot.hasError)
      return _buildErrorPage(snapshot.error);
    else
      return _buildLoadingPage();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.wait([_getFleetsItemsList(), _getCountry()]),
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return Scaffold(
            body: Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(mdRadiusCircular),
          ),
          elevation: 10,
          backgroundColor: Colors.transparent,
          child: GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            child: _getChild(context, snapshot),
          ),
        ));
      },
    );
  }
}
