import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:starlinx_support/api/auth_api.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/providers/auth.dart';
import 'package:starlinx_support/ui/widgets/widgets.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  late String _username, _password;

  @override
  Widget build(BuildContext context) {
    AuthProvider auth = Provider.of<AuthProvider>(context);
    final _userApi = locator<AuthApi>();

    final logoField = Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[Image.asset("asset/images/logo.png")]);
    final usernameField = TextFormField(
      autofocus: false,
      validator: (value) => value!.isEmpty ? "Please enter username" : null,
      // validator: validateEmail,
      onSaved: (value) => _username = value!,
      // initialValue: 'moshe123',
      // initialValue: 'ozdori',
      // initialValue: 'erm2020',

      // decoration: buildInputDecoration("erm2020", Icons.supervised_user_circle),
      decoration: buildInputDecoration(
          null, "Enter usaername", Icons.supervised_user_circle),
    );
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      validator: (value) => value!.isEmpty ? "Please enter password" : null,
      onSaved: (value) => _password = value!,
      // initialValue: 'engel123',
      //  initialValue: 'ozdori123',
      // initialValue: 'erm2020',

      // decoration: buildInputDecoration("erm2020", Icons.lock),

      decoration: buildInputDecoration(null, "Enter password", Icons.lock),
    );
    var loading = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        Text("Authenticating ... Please wait")
      ],
    );

    VoidCallback doLogin = () {
      final form = formKey.currentState;
      if (form!.validate()) {
        form.save();

        _userApi.login(_username, _password);
      } else {
        print("form is invalid");
      }
    };

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(40.0, 0, 40.0, 0),
          alignment: Alignment.center,
          child: Container(
            width: 400,
            child: Form(
              key: formKey,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // SizedBox(height: 60.0),
                  Spacer(),
                  logoField,
                  // Spacer(),
                  SizedBox(height: 60.0),
                  label("Username"),
                  SizedBox(height: 5.0),
                  usernameField,
                  SizedBox(height: 20.0),
                  label("Password"),
                  SizedBox(height: 5.0),
                  passwordField,
                  SizedBox(height: 20.0),
                  auth.loggedInStatus == Status.Authenticating
                      ? loading
                      : longButtons("Login", doLogin),
                  Spacer(),
                  //Spacer(),

                  // SizedBox(height: 5.0),
                  //forgotLabel
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
