import 'package:flutter/widgets.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:starlinx_support/app/app.locator.dart';
import 'package:starlinx_support/app/app.logger.dart';
import 'package:starlinx_support/app/app.router.dart';
import 'package:starlinx_support/services/user_service.dart';

class StartUpViewModel extends BaseViewModel {
  final log = getLogger('StartUpViewModel');
  final _userService = locator<UserService>();
  final _navigationService = locator<NavigationService>();

  Future<void> runStartupLogic(BuildContext context) async {
    await _userService.loadingData(context);

    Future.delayed(const Duration(milliseconds: 2000), () async {
      if (_userService.hasLoggedInUser) {
        log.v('We have a user session on disk. Sync the user profile ...');
        _navigationService.clearStackAndShow(Routes.homeView);
        // await Navigator.popAndPushNamed(context, Routes.homeView);

        // final currentUser = _userService.currentUser;
        // log.v('User sync complete. User profile: $currentUser');

        // log.v('We have a default address. Let\'s show them products!');
      } else {
        log.v('No user on disk, navigate to the LoginView');
        // await Navigator.popAndPushNamed(context, Routes.loginView);
        _navigationService.clearStackAndShow(Routes.loginView);
      }
    });
  }
}
