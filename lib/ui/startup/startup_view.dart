import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:stacked/stacked.dart';
import 'package:starlinx_support/ui/startup/startup_viewmodel.dart';

class StartUpView extends StatelessWidget {
  const StartUpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logoField = Container(
      padding: EdgeInsets.fromLTRB(40.0, 40.0, 40.0, 40.0),
      child: Image.asset("asset/images/startapp.png"),
    );

    return ViewModelBuilder<StartUpViewModel>.reactive(
      onModelReady: (model) =>
          SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
        model.runStartupLogic(context);
      }),
      builder: (context, model, child) => Scaffold(
        body: Center(
          child: logoField,
        ),
      ),
      viewModelBuilder: () => StartUpViewModel(),
    );
  }
}
