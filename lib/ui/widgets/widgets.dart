import 'package:flutter/material.dart';
import 'package:starlinx_support/ui/shared/app_colors.dart';
import 'package:starlinx_support/ui/shared/app_dimens.dart';
import 'package:starlinx_support/util/responsive.dart';

MaterialButton longButtons(String title, VoidCallback fun,
    {Color color: mcPrimaryColor, Color textColor: mcTextColorDark}) {
  return MaterialButton(
    onPressed: fun,
    textColor: textColor,
    color: color,
    child: SizedBox(
      width: double.infinity,
      child: Text(
        title,
        textAlign: TextAlign.center,
      ),
    ),
    height: 55,
    // minWidth: 300,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(mdRadiusCircular))),
  );
}

ElevatedButton elevatedButtons(
    BuildContext context, String title, VoidCallback fun,
    {Color color: mcPrimaryColor, Color textColor: mcTextColorDark}) {
  return ElevatedButton(
    onPressed: fun,
    style: TextButton.styleFrom(
      padding: EdgeInsets.symmetric(
        horizontal: mdDefaultPadding * 1.5,
        vertical: mdDefaultPadding / (Responsive.isDesktop(context) ? 1 : 2),
      ),
    ),
    child: Text(
      title,
      textAlign: TextAlign.center,
    ),
    // textColor: textColor,
    // color: color,
    // height: 55,
    // minWidth: 300,
    // shape: RoundedRectangleBorder(
    // borderRadius: BorderRadius.all(Radius.circular(mdRadiusCircular))),
  );
}

label(String title) => Text(title);
labeWhite(String title, FontWeight? fw, double? fontSize) => Text(title,
    style: TextStyle(color: Colors.black, fontSize: fontSize, fontWeight: fw));

InputDecoration buildInputDecoration(
    String? labelText, String? hintText, IconData? icon) {
  double peddingLR = (icon != null ? 20.0 : 15.0);
  return InputDecoration(
      prefixIcon: (icon != null ? Icon(icon) : null),
      // value: hintText,
      labelText: labelText,
      hintText: hintText,
      contentPadding: EdgeInsets.fromLTRB(peddingLR, 15.0, peddingLR, 15.0));
}

InputDecoration buildInputDecorationWhite(String? labelText, String? hintText,
    IconData? icon, IconData? suffix, VoidCallback? funSuffix) {
  double peddingLR = (icon != null ? 20.0 : 15.0);
  return InputDecoration(
      prefixIcon: (icon != null ? Icon(icon) : null),
      suffix: (suffix != null
          ? IconButton(
              icon: Icon(suffix, color: mcTextColorDark), onPressed: funSuffix)
          : null),
      // value: hintText,
      labelText: labelText,
      labelStyle: TextStyle(color: mcTextColorDark),
      hintText: hintText,
      hintStyle: TextStyle(color: mcTextColorDark),
      suffixStyle: TextStyle(color: mcTextColorDark),
      fillColor: mcTextColorDark,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(mdRadiusCircular),
        borderSide: BorderSide(color: mcTextColorDark),
      ),
      contentPadding: EdgeInsets.fromLTRB(peddingLR, 15.0, peddingLR, 15.0));
}

InputDecoration buildInputDecorationBorderBottomWhite(
    String? labelText,
    String? hintText,
    IconData? icon,
    IconData? suffix,
    VoidCallback? funSuffix) {
  double peddingLR = (icon != null ? 20.0 : 15.0);
  return InputDecoration(
      // filled: true,
      prefixIcon: (icon != null ? Icon(icon) : null),
      suffixIcon: (suffix != null
          ? Container(
              height: 28,
              child: IconButton(
                  color: mcTextColorDark,
                  iconSize: 24,
                  padding: EdgeInsets.all(0.0),
                  icon: Icon(suffix, color: mcTextColorDark),
                  onPressed: funSuffix),
            )
          : null),
      // value: hintText,
      labelText: labelText,
      labelStyle: TextStyle(color: mcTextColorDark),
      hintText: hintText,
      hintStyle: TextStyle(color: mcTextColorDark),

      ///suffixStyle: TextStyle(color: mcTextColorDark),
      // fillColor: Colors.white,
      border: UnderlineInputBorder(
        // borderRadius: BorderRadius.circular(mdRadiusCircular),
        borderSide: BorderSide(color: mcTextColorDark),
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: mcTextColorDark),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: mcTextColorDark),
      ),
      contentPadding: EdgeInsets.fromLTRB(peddingLR, 15.0, peddingLR, 15.0));
}

InputDecoration buildDropdownDecorationWhite(
    String? labelText, String? hintText, IconData? icon) {
  double peddingLR = (icon != null ? 20.0 : 15.0);
  return InputDecoration(
      isDense: false,
      prefixIcon: (icon != null ? Icon(icon) : null),
      // value: hintText,
      labelText: labelText,
      labelStyle: TextStyle(color: mcTextColorDark),
      hintText: hintText,
      hintStyle: TextStyle(color: mcTextColorDark),
      suffixStyle: TextStyle(color: mcTextColorDark),
      fillColor: mcTextColorDark,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(mdRadiusCircular),
        borderSide: BorderSide(color: mcTextColorDark),
      ),
      contentPadding: EdgeInsets.fromLTRB(peddingLR, 15.0, peddingLR, 15.0));
}
